/*! @file
 *
 *  @brief Routines for controlling the Real Time Clock (RTC) on the TWR-K70F120M.
 *
 *  This contains the functions for operating the real time clock (RTC).
 *
 *  @author BAllen, APope
 *  @date 2015-08-24
 */
/*!
**  @addtogroup RTC_module RTC module documentation
**  @{
*/
/* MODULE RTC */
#include "MK70F12.h"
#include "RTC.h"
#include "OS.h"

static void (*ISRCallBack)(void *);	  /*!< The callback function that the RTC ISR will invoke */
static void *CallBackArguments;		    /*!< The arguments to pass to the ISRCallBack function */

BOOL RTC_Init(void (*userFunction)(void *), void *userArguments)
{
  int i;
  uint8_t hours, seconds, minutes;
  uint32_t totalSeconds;

  //Assign the Callback
  ISRCallBack = userFunction;
  CallBackArguments = userArguments;

  //Enable Clock gating
  SIM_SCGC6 |= SIM_SCGC6_RTC_MASK;

  //Attempt to assert SWR flag to check if the control register is locked
  RTC_CR = RTC_CR_SWR_MASK;

  if (RTC_CR == RTC_CR_SWR_MASK)
  {
    RTC_CR &= ~RTC_CR_SWR_MASK; //Software Reset: 0 No effect

    RTC_TSR = 0;                //Clear the TIF flag

    //Setup the RTC control register
    RTC_CR |= RTC_CR_SC2P_MASK;   //Oscillator 2pF Load Configure: 1 enabled
    RTC_CR &= ~RTC_CR_SC4P_MASK;  //Oscillator 4pF Load Configure: 0 disabled
    RTC_CR &= ~RTC_CR_SC8P_MASK;  //Oscillator 8pF Load Configure: 0 disabled
    RTC_CR |= RTC_CR_SC16P_MASK;  //Oscillator 16pF Load Configure: 1 enabled

    RTC_CR &= ~RTC_CR_CLKO_MASK;  //Clock Output: 0 The 32 kHz clock is output to no other peripherals
    RTC_CR |= RTC_CR_OSCE_MASK;   //Oscillator Enable: 1 32.768 kHz oscillator is enabled

    RTC_CR &= ~RTC_CR_UM_MASK;    //Update Mode: 0 Registers cannot be written when locked
    RTC_CR &= ~RTC_CR_SUP_MASK;   //Supervisor Access: 0 Non-supervisor mode write accesses are not supported

    RTC_CR &= ~RTC_CR_WPE_MASK;   //Wakeup Pin Enable: 0 Wakeup pin is disabled

    //Wait an arbitrary amount of time for the oscillator to become stable before enabling the time counter
    for(i = 0; i < 9000; i++);

    RTC_LR &= ~RTC_LR_CRL_MASK; //Control Register Lock: 0 is locked and writes are ignored
  }

  //Setup the RTC_LR (Lock Register)
  RTC_LR &= ~RTC_LR_MCHL_MASK;	//Monotonic Counter High Lock: 0 is locked and writes are ignored
  RTC_LR &= ~RTC_LR_MCLL_MASK;	//Monotonic Counter Low Lock: 0 is locked and writes are ignored
  RTC_LR &= ~RTC_LR_MEL_MASK;	//Monotonic Enable Lock: 0 is locked and writes are ignored
  RTC_LR &= ~RTC_LR_TTSL_MASK;	//Tamper Time seconds Lock: 0 is locked and writes are ignored
  RTC_LR &= ~RTC_LR_LRL_MASK;	//Lock Register Lock: 0 is locked and writes are ignored
  RTC_LR |= RTC_LR_SRL_MASK;	//Status Register Lock: 1 writes complete as normal
  RTC_LR &= ~RTC_LR_TCL_MASK;	//Time compensation Lock: 0 is locked and writes are ignored

  //Init NVIC
  NVICICPR2 = (1 << 3); //Clear pending interrupts on RTC
  NVICISER2 = (1 << 3); //Enable interrupts from RTC module

  //Enable Time Seconds Interrupt and Disable the others
  RTC_IER |= RTC_IER_TSIE_MASK;
  RTC_IER &= ~RTC_IER_TAIE_MASK;
  RTC_IER &= ~RTC_IER_TOIE_MASK;
  RTC_IER &= ~RTC_IER_TIIE_MASK;

  RTC_SR |= RTC_SR_TCE_MASK;
  return (bTRUE);
}

void RTC_Set(const uint8_t hours, const uint8_t minutes, const uint8_t seconds)
{
  uint32_t totalSeconds = seconds + (minutes * 60) + (hours * 60 * 60);

  //Disable the Time counter
  RTC_SR &= ~RTC_SR_TCE_MASK;

  //Write the new time value to RTC_TSR
  RTC_TSR = totalSeconds;

  //Re-enable the Time Counter
  RTC_SR |= RTC_SR_TCE_MASK;
}

void RTC_Get(uint8_t * const hours, uint8_t * const minutes, uint8_t * const seconds)
{
  uint32_t Read1, Read2;
  uint8_t days;
  BOOL timeMatch = bFALSE;

  /* Reading the timer counter while incrementing may return invalid data due to sync issues. We must perform
   * two read accesses and confirm that the same data was returned for both reads.
   */
  while (!timeMatch)
  {
    Read1 = RTC_TSR; Read2 = RTC_TSR;
    timeMatch = (Read1 == Read2);
  }

  RTC_CalcTime(Read1, &days, hours, minutes, seconds);
}

void RTC_CalcTime(uint32_t totalSeconds, uint8_t * const days, uint8_t * const hours, uint8_t * const minutes, uint8_t *const seconds)
{
  *days    = ((totalSeconds / 86400));
  *hours   = ((totalSeconds / 3600) % 24);
  *minutes = ((totalSeconds % 3600) / 60);
  *seconds = ((totalSeconds % 3600) % 60);
}

void __attribute__ ((interrupt)) RTC_ISR(void)
{
  OS_ISREnter();
  ISRCallBack(CallBackArguments);       //Invoke the ISR Callback function
  OS_ISRExit();
}
/* END RTC */
/*!
** @}
*/
