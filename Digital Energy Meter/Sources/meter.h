/*! @file
 *
 *  @brief Maintains usage statistics of the DEM.
 *
 *  This module contains the routines that monitors usage.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef METER_H
#define METER_H

#include "types.h"

extern int64_t Meter_TotalEnergy;     /*!< Total recorded energy the DEM has measured since power-up. */
extern int64_t Meter_TotalEnergyCost; /*!< Running-cost of the energy the DEM has consumed since power-up. */
extern uint16_t Meter_PowerFactor;    /*!< The current power factor. */
extern uint16_t Meter_VoltageRMS;     /*!< Voltage RMS value (V).  Note: This is real value (no base) */
extern uint16_t Meter_CurrentRMS;     /*!< Current RMS value (mA). Note: This is real value (no base) */
extern uint32_t Meter_Time;           /*!< The amount of time the DEM has been running for (seconds). */

/*! @brief Initializes the Meter before first use.
 *
 *  @return BOOL - TRUE if initialization was successful.
 */
BOOL Meter_Init(void);

/*! @brief Thread that performs meter calculations when data becomes available.
 *
 *  @param pData - Generic pointer to pass data into the thread.
 *  @return void
 */
void Meter_RunCalculations(void *pData);

/*! @brief Thread that gets samples from the sampler module at specific intervals.
 *
 *  @param pData - Generic pointer to pass data into the thread.
 *  @return void
 */
void Meter_GetSamples(void *pData);

/*! @brief Increments the DEM timer.
 *
 *  @return void
 *  @note Should only be invoked by a seconds interrupt.
 */
void Meter_IncrementTime(void);
#endif
