/*! @file
 *
 *  @brief I/O routines for UART communications on the TWR-K70F120M.
 *
 *  This contains the functions for operating the UART (serial port).
 *
 *  @author BAllen, APope
 *  @date 2015-08-15
 */
/*!
**  @addtogroup UART_module UART module documentation
**  @{
*/
/* MODULE UART */
#include "MK70F12.h"
#include "Cpu.h"
#include "bits.h"
#include "FIFO.h"
#include "UART.h"
#include "OS.h"

static TFIFO TxFIFO;              /*!< Transmitter FIFO buffer */
static TFIFO RxFIFO;              /*!< Receiver FIFO buffer */
static OS_ECB *BufferAvailable;   /*!< Semaphore to indicate when OutChar is available */

BOOL UART_PrintDriver(uint8_t const * const buffer, uint8_t length)
{
  uint8_t i;
  BOOL sendOk = bTRUE;

  OS_SemaphoreWait(BufferAvailable, 0);
  for (i = 0; i < length; i++)
  {
    sendOk &= UART_OutChar(buffer[i]);
  }
  OS_SemaphoreSignal(BufferAvailable);

  return (sendOk);
}

BOOL UART_Init(const uint32_t baudRate, const uint32_t moduleClk)
{
  uint8_t brfa;             //Baud Rate Fractional Adjuster
  uint16union_t sbr;
  BOOL initOk = bFALSE;

  BufferAvailable = OS_SemaphoreCreate(1);    //Initialise the semaphore

  if (baudRate != 0) //Sanity check, Cannot Divide by zero in the baud rate equation below
  {
    //Integer division will automatically truncate anything after the decimal point, therefore we can simply find the SBR:
    sbr.l = (moduleClk / (16 * baudRate));

    if (sbr.l < 0x1FFF) //Register size to hold SBR is 13 bits long only, therefore it cannot be greater than 0x1FFF
    {
      //As we are not utilizing the FPU yet, we shall scale by a factor of 32 to avoid floating point numbers
      brfa = ((moduleClk*2) / (baudRate)) - ((sbr.l * 32));

      //Enable UART2 and PORTE through System Clock Gating Registers
      SIM_SCGC4 |= SIM_SCGC4_UART2_MASK;
      SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK;

      //Set both PORTE_PCR16/17 to (Alternative 3): UART2_TX/UART2_RX
      PORTE_PCR16 &= ~PORT_PCR_MUX_MASK;
      PORTE_PCR17 &= ~PORT_PCR_MUX_MASK;

      // Set MUX to 3
      PORTE_PCR16 |= PORT_PCR_MUX(3);
      PORTE_PCR17 |= PORT_PCR_MUX(3);

      //Set SBR[12:8] in the UART2_BDH Register
      UART2_BDH |= UART_BDH_SBR_MASK;
      UART2_BDH &= (sbr.s.Hi |= ~(UART_BDH_SBR_MASK)); 	//Put the higher 5 bits of SBR into the lower 5 bits of the UART2_BDH register

      //Set SBR[7:0] in the UART2_BDL Register
      UART2_BDL |= 0xFF;
      UART2_BDL &= sbr.s.Lo; 				//Put the remainder 8 bits of SBR into the UART2_BDL register

      //Set BRFA in the UART2_C4 Register
      UART2_C4 |= UART_C4_BRFA_MASK;
      UART2_C4 &= (brfa |= ~(UART_C4_BRFA_MASK));	//Put the lower 5 bits of BRFA into the lower 5 bits of the UART2_C4 register

      //Set Control Register 1
      UART2_C1 &= ~UART_C1_LOOPS_MASK; 		//LOOPS; 0:Normal operation
      UART2_C1 &= ~UART_C1_UARTSWAI_MASK; 	//UARTSWAI; 0:UART clock continues to run in Wait mode
      UART2_C1 &= ~UART_C1_RSRC_MASK; 		//RSRC; 0:Selects internal loop back mode. Receiver input is internally connected to transmitter output
      UART2_C1 &= ~UART_C1_M_MASK; 		//M; 0:Normal-start+ 8 data bits + stop
      UART2_C1 &= ~UART_C1_WAKE_MASK; 		//WAKE; 0:Idle line wakeup
      UART2_C1 &= ~UART_C1_ILT_MASK; 		//ILT; 0:Idle character bit count starts after start bit
      UART2_C1 &= ~UART_C1_PE_MASK; 		//PE; 0:Parity function disabled
      UART2_C1 &= ~UART_C1_PT_MASK; 		//PT; 0:Even Parity

      //Set Control Register 2
      UART2_C2 &= ~UART_C2_TIE_MASK; 		//TIE; 0:TDRE Interrupt and DMA transfer requests disabled (initially upon startup)
      UART2_C2 &= ~UART_C2_TCIE_MASK; 		//TCIE; 0:TC interrupt requests disabled
      UART2_C2 |= UART_C2_RIE_MASK; 		//RIE; 1:RDRF interrupt and DMA transfer request enabled
      UART2_C2 &= ~UART_C2_ILIE_MASK; 		//ILIE; 0:IDLE interrupt requests disabled
      UART2_C2 |= UART_C2_TE_MASK;   		//TE; 1:Transmitter on
      UART2_C2 |= UART_C2_RE_MASK;   		//RE; 1:Receiver on
      UART2_C2 &= ~UART_C2_RWU_MASK; 		//RWU; 0:Normal operation
      UART2_C2 &= ~UART_C2_SBK_MASK ; 		//SBK; 0:Normal transmitter operation

      //Set Control Register 3
      UART2_C3 &= ~UART_C3_TXDIR_MASK; 		//TXDIR; 0:TXD pin is an input in single wire mode
      UART2_C3 &= ~UART_C3_TXINV_MASK; 		//TXINV; 0:Transmit data is not inverted
      UART2_C3 &= ~UART_C3_ORIE_MASK; 		//ORIE; 0:OR interrupts are disabled
      UART2_C3 &= ~UART_C3_NEIE_MASK; 		//NEIE; 0:NF interrupt requests are disabled
      UART2_C3 &= ~UART_C3_FEIE_MASK; 		//FEIE; 0:FE interrupt requests are disabled
      UART2_C3 &= ~UART_C3_PEIE_MASK; 		//PEIE; 0:PF interrupt requests are disabled

      //Set Control Register 4
      UART2_C4 &= ~UART_C4_MAEN1_MASK; 		//MAEN1; 0:All data received is transferred to the data buffer if MAEN2 is cleared
      UART2_C4 &= ~UART_C4_MAEN2_MASK; 		//MAEN2; 0:All data received is transferred to the data buffer if MAEN1 is cleared

      //Set Control Register 5
      UART2_C5 &= ~UART_C5_TDMAS_MASK; 		//TDMAS; 0:If C2[TIE] & S1[TDRE] are not set, TDRE interrupt request signal can assert
      UART2_C5 &= ~UART_C5_RDMAS_MASK; 		//RDMAS; 0:If C2[RIE] & S1[RDRF] are not set, RDRF interrupt request signal can assert

      //Set MODEM Register
      UART2_MODEM &= ~UART_MODEM_RXRTSE_MASK; 	//RXRTSE; 0:The receiver has no effect on RTS
      UART2_MODEM &= ~UART_MODEM_TXRTSPOL_MASK; //TXRTSPOL; 0:Transmitter RTS is active low
      UART2_MODEM &= ~UART_MODEM_TXRTSE_MASK; 	//TXRTSE; 0:The transmitter has no effect on RTS
      UART2_MODEM &= ~UART_MODEM_TXCTSE_MASK; 	//TXCTSE; 0:CTS has no effect on the transmitter

      //Initialize TxFIFO and RxFIFO
      FIFO_Init(&TxFIFO);
      FIFO_Init(&RxFIFO);

      //Initialize NVIC for the UART2 module
      NVICICPR1 = (1 << 17); //Clear pending interrupts on UART2
      NVICISER1 = (1 << 17); //Enable interrupts from UART2 module

      initOk = bTRUE;
    }
  }

  return (initOk);
}

BOOL UART_InChar(uint8_t * const dataPtr)
{
  return (FIFO_Get(&RxFIFO, dataPtr));
}
 
BOOL UART_OutChar(const uint8_t data)
{
  OS_DisableInterrupts();			      //A critical section occurs here

  if (FIFO_Put(&TxFIFO, data))
  {
    UART2_C2 |= UART_C2_TIE_MASK; 	//ARM OUTPUT

    OS_EnableInterrupts();
    return (bTRUE);
  }

  OS_EnableInterrupts();
  return (bFALSE);
}

void __attribute__ ((interrupt)) UART_ISR(void)
{
  OS_ISREnter();

  if ((UART2_S1 & UART_S1_TDRE_MASK) && (UART2_C2 & UART_C2_TIE_MASK)) 	//Check if the TDRE and TIE flag is set
  {
    if (!FIFO_Get(&TxFIFO, (uint8_t * const)&UART2_D))
      UART2_C2 &= ~UART_C2_TIE_MASK;	//DISARM OUTPUT
  }

  if ((UART2_S1 & UART_S1_RDRF_MASK) && (UART2_C2 & UART_C2_RIE_MASK))	//Check if the RDRF and RIE flag is set
  {
    FIFO_Put(&RxFIFO, UART2_D);
  }

  OS_ISRExit();
}
/* END UART */
/*!
** @}
*/
