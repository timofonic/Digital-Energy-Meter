/*! @file
 *
 *  @brief Routines to implement a FIFO buffer.
 *
 *  This contains the structure and "methods" for accessing a byte-wide FIFO.
 *
 *  @author BAllen, APope
 *  @date 2015-08-15
 */
/*!
**  @addtogroup FIFO_module FIFO module documentation
**  @{
*/
/* MODULE FIFO */
#include "Cpu.h"
#include "FIFO.h"
#include "OS.h"

void FIFO_Init(TFIFO * const FIFO)
{
  FIFO->Start = 0;
  FIFO->End = 0;
  FIFO->NbBytes = 0;
}

BOOL FIFO_Put(TFIFO * const FIFO, const uint8_t data)
{
  OS_DisableInterrupts();                     //Enter Critical Section

  if (FIFO->NbBytes < FIFO_SIZE)              //Ensure that the buffer is not full before putting data within it
  {
    FIFO->NbBytes++; 				                  //Increment data counter
    FIFO->Buffer[FIFO->End] = data; 		      //Store data in the buffer
    FIFO->End = (FIFO->End + 1) % FIFO_SIZE; 	//increment end pointer within FIFO_SIZE

    OS_EnableInterrupts();
    return (bTRUE);
  }

  OS_EnableInterrupts();
  return (bFALSE);
}

BOOL FIFO_Get(TFIFO * const FIFO, uint8_t * const dataPtr)
{
  OS_DisableInterrupts();                         //Enter Critical Section

  if (FIFO->NbBytes != 0)
  {
    FIFO->NbBytes--;
    *dataPtr = FIFO->Buffer[FIFO->Start];
    FIFO->Start = (FIFO->Start + 1) % FIFO_SIZE;

    OS_EnableInterrupts();
    return (bTRUE);
  }

  OS_EnableInterrupts();
  return (bFALSE);
}
/* END FIFO */
/*!
** @}
*/
