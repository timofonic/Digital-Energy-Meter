/*! @file
 *
 *  @brief Retrieves samples from the DAC (Digital to Analog Converter).
 *
 *  This module contains the routines that take samples from the current and voltage waveforms.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef SAMPLER_H
#define SAMPLER_H

#include "types.h"

extern int16_t Sampler_VoltageSample; /*!< The most recent voltage sample. */
extern int16_t Sampler_CurrentSample; /*!< The most recent current sample. */

/*! @brief Initializes the Sampler before first use.
 *
 *  @param userFunction  - Callback function that invokes at the same frequency as the sampler.
 *  @param userArguments - The arguments to the call-back function.
 *  @return BOOL - TRUE if initialization was successful.
 */
BOOL Sampler_Init(void (*userFunction)(void *), void *userArguments);
#endif
