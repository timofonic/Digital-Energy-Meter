/*! @file
 *
 *  @brief Controls the tariffs of the DEM.
 *
 *  Provides tariff information depending on user selection.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup TARIFF_module TARIFF module documentation
**  @{
*/
/* MODULE TARIFF */
#include "tariff.h"
#include "Flash.h"
#include "RTC.h"

volatile uint8_t *NvTariffMode;    /*!< Address of the Tariff Mode in Flash Memory. */

static const int32_t TARRIF_1_PEAK       = 22235; //(1000)*22.235; -> cents/kWh
static const int32_t TARRIF_1_SHOULDER   = 4400;  //(1000)*4.400;  -> cents/kWh
static const int32_t TARRIF_1_OFF_PEAK   = 2109;  //(1000)*2.109;  -> cents/kWh

static const int32_t TARRIF_2            = 1713;  //(1000)*1.713;  -> cents/kWh
static const int32_t TARRIF_3            = 4100;  //(1000)*4.100;  -> cents/kWh

BOOL Tariff_Init(void)
{
  BOOL initOk = bFALSE;

  //Attempt to initialize flash and check that the tariff is not set to default
  if (Flash_Init())
  {
    if (Flash_AllocateVar((volatile void **)&NvTariffMode, sizeof((*NvTariffMode))))
    {
      if ((*NvTariffMode) == 0xFF)
      {
        //Default mode will be tariff mode 1
        initOk = Flash_Write8(NvTariffMode, TARIFF_MODE_1);
      }
      else
      {
        initOk = bTRUE;
      }
    }
  }

  return (initOk);
}

BOOL Tariff_SetTariff(TTariff tariffMode)
{
  return (Flash_Write8(NvTariffMode, tariffMode));
}

int32_t getTariffBasedOnTime(uint32_t time)
{
  int32_t tariffCost = 0;
  uint8_t days, hours, minutes, seconds;

  RTC_CalcTime(time, &days, &hours, &minutes, &seconds);

  if (hours > 14 && hours < 20)
  {
    //We are in Peak
    tariffCost = TARRIF_1_PEAK;
  }
  else if ((hours > 7 && hours < 14) || (hours > 20 && hours < 22))
  {
    //We are in shoulder
    tariffCost = TARRIF_1_SHOULDER;
  }
  else
  {
    //We are in Off-Peak
    tariffCost = TARRIF_1_OFF_PEAK;
  }

  return (tariffCost);
}

int32_t Tariff_GetTariffCost(uint32_t time)
{
  int32_t tariffCost = 0;

  switch ((*NvTariffMode))
  {
    case TARIFF_MODE_1:
      tariffCost = getTariffBasedOnTime(time);
      break;
    case TARIFF_MODE_2:
      tariffCost = TARRIF_2;
      break;
    case TARIFF_MODE_3:
      tariffCost = TARRIF_3;
      break;
  }

  return (tariffCost);
}
/* END TARIFF */
/*!
** @}
*/
