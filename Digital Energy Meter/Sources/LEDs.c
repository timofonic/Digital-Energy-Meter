/*! @file
 *
 *  @brief Routines to access the LEDs on the TWR-K70F120M.
 *
 *  This contains the functions for operating the LEDs.
 *
 *  @author BAllen, APope
 *  @date 2015-08-15
 */
/*!
**  @addtogroup LED_module LED module documentation
**  @{
*/
/* MODULE LED */
#include "MK70F12.h"
#include "LEDs.h"

BOOL LEDs_Init(void)
{
  SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK; //Enable PORTA clock for GPIO
  
  //Initialize the state of the output pins (logic High is off)
  GPIOA_PSOR |= (LED_ORANGE | LED_BLUE | LED_YELLOW | LED_GREEN);

  //Set PORTA_PCR10, 11, 28, 29 to (Alternative 1): General purpose I/O Pin
  PORTA_PCR(11) &= ~PORT_PCR_MUX_MASK;
  PORTA_PCR(28) &= ~PORT_PCR_MUX_MASK;
  PORTA_PCR(29) &= ~PORT_PCR_MUX_MASK;
  PORTA_PCR(10) &= ~PORT_PCR_MUX_MASK;

  PORTA_PCR(11) |= PORT_PCR_MUX(1); //ORANGE
  PORTA_PCR(28) |= PORT_PCR_MUX(1); //YELLOW
  PORTA_PCR(29) |= PORT_PCR_MUX(1); //GREEN
  PORTA_PCR(10) |= PORT_PCR_MUX(1); //BLUE

  //Set the direction for each pin (1: output)
  GPIOA_PDDR |= (LED_ORANGE | LED_BLUE | LED_YELLOW | LED_GREEN);

  return (bTRUE);
}

void LEDs_On(const TLED colour)
{
  GPIOA_PCOR |= colour;
}

void LEDs_Off(const TLED colour)
{
  GPIOA_PSOR |= colour;
}

void LEDs_Toggle(const TLED colour)
{
  GPIOA_PTOR |= colour;
}

void LEDs_AllOff(void)
{
  LEDs_Off(LED_YELLOW);
  LEDs_Off(LED_ORANGE);
  LEDs_Off(LED_GREEN);
  LEDs_Off(LED_BLUE);
}

void LEDs_ResetToTowerInit(void)
{
  LEDs_Off(LED_YELLOW);
  LEDs_On(LED_ORANGE);		//Orange LED indicates successful tower init
  LEDs_Off(LED_GREEN);
  LEDs_Off(LED_BLUE);
}

/* END LED */
/*!
** @}
*/
