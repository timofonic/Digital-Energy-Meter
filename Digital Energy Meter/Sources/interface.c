/*! @file
 *
 *  @brief Acts as a human machine interface between the DEM and PC.
 *
 *  This module contains the routines that process user requests.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup INTERFACE_module INTERFACE module documentation
**  @{
*/
/* MODULE INTERFACE */
#include "Cpu.h"
#include "bits.h"
#include "RTC.h"
#include "Flash.h"
#include "LEDs.h"
#include "FTM.h"
#include "debounce.h"
#include "packet.h"
#include "DAC.h"
#include "meter.h"
#include "interface.h"
#include "tariff.h"
#include "displayprint.h"
#include "OS.h"

#define BAUD_RATE 115200            //Baud Rate used to initialize UART2 module

//Defining the various PC to Tower packet commands
//Basic Protocol extension
#define TEST_MODE_COMMAND 0x10
#define TARIFF_COMMAND    0x11
#define TIME1_COMMAND     0x12
#define TIME2_COMMAND     0x13
#define POWER_COMMAND     0x14
#define ENERGY_COMMAND    0x15
#define COST_COMMAND      0x16

//Intermediate Protocol extension
#define FREQUENCY_COMMAND 0x17
#define VOLT_RMS_COMMAND  0x18
#define CURR_RMS_COMMAND  0x19
#define PFACTOR_COMMAND   0x1A

//Protocols for changing the self-test waveforms
#define VOLT_AMP_COMMAND  0x1B
#define CURR_AMP_COMMAND  0x1C
#define PHASE_COMMAND     0x1D

typedef struct
{
  uint32_t base;
  uint8_t n;
}TBASE;

const uint8_t PACKET_ACK_MASK = 0x80;     /*!< The helps determine whether the PC requires ACK upon arrival */

//Voltage Waveform
const uint16_t VOLTAGE_MAX_VALUE = 11584;  /*!< Max value of the voltage self-test waveform BASE OF 1000/2^15 */
const uint16_t VOLTAGE_MIN_VALUE = 9266;   /*!< Min value of the voltage self-test waveform BASE OF 1000/2^15 */
//Current Waveform
const uint16_t CURRENT_MAX_VALUE = 23174;  /*!< Max value of the voltage self-test waveform BASE OF 10/2^15 */
const uint16_t CURRENT_MIN_VALUE = 0;      /*!< Min value of the voltage self-test waveform BASE OF 1000/2^15 */

//Note, energy base has an additional scaling factor of 3600 - as the DEM is always in accelerated time mode where kWs = kWh.
const TBASE ENERGY_BASE     = { /*!< Base of Meter_TotalEnergy (kWh) (10*1000*(1.25mS*3600)/2^30*1000*3600) = 125 ((32Q30)*10,000). */
    .base = 125,
    .n = 30
};

const TBASE COST_TOTAL_BASE = { /*!< Base of Meter_TotalEnergyCost (10*1.25mS/2^30*1000) = 125 ((32Q30)*1x10^7). */
    .base = 125,
    .n = 30
};

//FTM timer structure for the display timeout
void displayDormantCallBack(void * nothing);
static const TFTMChannel DISPLAY_DORMANT_TIMER = {
    .channelNb              = 0,
    .delayCount             = 24414, //1s
    .timerFunction          = TIMER_FUNCTION_OUTPUT_COMPARE,
    .ioType.outputAction    = TIMER_OUTPUT_LOW,
    .ioType.inputDetection  = TIMER_INPUT_OFF,
    .userFunction           = displayDormantCallBack,
    .userArguments          = 0
};

static uint8_t  DormantDisplayTimerCounter;   /*!< Counts the number of times the DISPLAY_DORMANT_TIMER has triggered */
static OS_ECB *Switch1Pressed;                /*!< Semaphore used to indicate when switch 1 has been pressed. */
static OS_ECB *PrintToTerminal;               /*!< Semaphore used to indicate that data needs to be printed to the terminal. */

//De-bounce structure for Switch 1
void cycleDisplayCallBack(void * nothing);
static TDebounce Switch1 = {
  .buttonID = BUTTON_SW1,
  .debounceCompleteCallbackFunction = cycleDisplayCallBack,
  .debounceCompleteCallbackArguments = 0
};

typedef enum {
  DISPLAY_DORMANT,
  DISPLAY_METER_TIME,
  DISPLAY_AVERAGE_POWER,
  DISPLAY_TOTAL_ENERGY,
  DISPLAY_TOTAL_COST
}TDisplayState;

static TDisplayState CurrentDisplay;    /*!< The current Display state of the terminal. */

/*! @brief Call-back for the Display dormant counter.
 *
 */
void displayDormantCallBack(void * nothing)
{
  OS_DisableInterrupts();
  if (DormantDisplayTimerCounter == 15)
  {
    //The user has not activated switch1 in 15 seconds, reset the display to the dormant state
    DormantDisplayTimerCounter = 1;
    CurrentDisplay = DISPLAY_DORMANT;
  }
  else
  {
    DormantDisplayTimerCounter++;
    FTM_StartTimer(&DISPLAY_DORMANT_TIMER); //Restart the timer
  }
  OS_EnableInterrupts();
}

/*! @brief Call-back for switch 1.
 *
 *  This will cycle the state machine, and change the currently displaying quantity.
 */
void cycleDisplayCallBack(void * nothing)
{
  OS_SemaphoreSignal(Switch1Pressed);
}

/*! @brief Using the base definition it will normalise and calculate AveragePower
 *
 *  @param integerPart - Stores the Integer part of Average Power
 *  @param fractionalPart - Stores the fractional part of Average Power
 *  @param convertToKiloWatts - Bool is set if user wants the answer in KilloWatts
 */
static void normalisePower(uint16_t * const integerPart, uint16_t * const fractionalPart, BOOL convertToKiloWatts)
{
  uint64_t result;

  OS_DisableInterrupts();
  uint16_t voltRMS = Meter_VoltageRMS;
  uint16_t currRMS = Meter_CurrentRMS;
  uint32_t pf = DAC_PowerFactor();
  OS_EnableInterrupts();

  result = (uint64_t) voltRMS * currRMS * pf;
  result = (result >> 16);  //Remove scaling factor from the PowerFactor

  //Calculate the integer part
  if (convertToKiloWatts)
  {
    *integerPart = (uint16_t)(result/1000000);  //Converting to kW (and removing currentRMS base)
  }
  else
  {
    *integerPart = (uint16_t)(result/1000); //Remove the scaling factor within current RMS
  }

  //Calculate the fractional part
  result = (uint64_t) voltRMS * currRMS * pf * 1000;
  result = (result >> 16);

  if (convertToKiloWatts)
  {
    *fractionalPart = (uint16_t)((result/1000000) - ((*integerPart)*1000));
  }
  else
  {
    *fractionalPart = (uint16_t)((result/1000) - ((uint32_t)(*integerPart)*1000));
  }
}

/*! @brief Using the base definition it will normalize and calculate Energy (kWh)
 *
 *  @param integerPart - Stores the Integer part of Energy
 *  @param fractionalPart - Stores the fractional part of Energy
 */
static void normaliseEnergy(uint16_t * const integerPart, uint16_t * const fractionalPart)
{
  uint64_t result;
  int64_t meterEnergy = Meter_TotalEnergy;  //Copy Global variable into local

  //Calculate Whole part
  result      = meterEnergy * (ENERGY_BASE.base);
  result      = (result >> ENERGY_BASE.n);
  *integerPart = (uint16_t)(result / 10000);  //Scale down again (extra scaling factor in base equation)

  //Calculate Fraction
  result      = meterEnergy * 1000 * (ENERGY_BASE.base);
  result      = (result >> ENERGY_BASE.n);
  *fractionalPart  = (uint16_t)((uint64_t)((result / 10000) - ((*integerPart)*1000)));
}

/*! @brief Using the base definition it will normalize and calculate Cost
 *
 *  @param dollars - Stores the $$ of cost
 *  @param cents   - Stores the cc of cost
 */
static void normaliseCost(uint16_t * const dollars, uint8_t * const cents)
{
  uint64_t totalCents;

  totalCents = Meter_TotalEnergyCost * (COST_TOTAL_BASE.base);
  totalCents = (totalCents >> COST_TOTAL_BASE.n);
  totalCents = totalCents / 10000000;             //Account for scaling factor in base equation

  //Calculate Dollars
  *dollars  = (uint16_t)(totalCents/100);

  //Calculate Cents
  *cents    = (uint8_t)(totalCents - ((*dollars)*100));
}

/*! @brief Print Time to the terminal.
 *
 */
static void printTime(void)
{
  uint8_t days, hours, minutes, seconds;
  RTC_CalcTime(Meter_Time, &days, &hours, &minutes, &seconds);
  DisplayPrint_Time(days, hours, minutes, seconds);
}

/*! @brief Print Power to the terminal.
 *
 */
static void printPower(void)
{
  uint16_t powerWhole, powerFraction;
  normalisePower(&powerWhole, &powerFraction, bTRUE);
  DisplayPrint_Power(powerWhole, powerFraction);
}

/*! @brief Print energy to the terminal.
 *
 */
static void printEnergy(void)
{
  uint16_t energyWhole, energyFraction;
  normaliseEnergy(&energyWhole, &energyFraction);
  DisplayPrint_Energy(energyWhole, energyFraction);
}

/*! @brief Print cost to the terminal.
 *
 */
static void printCost(void)
{
  uint16_t dollars;
  uint8_t cents;

  normaliseCost(&dollars, &cents);
  DisplayPrint_Cost(dollars, cents);
}

/*! @brief Call-back for the RTC module.
 *
 *  This will trigger every second, update the user display if it is not dormant, and send time information.
 */
static void secondsCallBack(void * nothing)
{
  Meter_IncrementTime();              //Increment the meter timer
  OS_SemaphoreSignal(PrintToTerminal);
}

/*! @brief Responds to a TEST_MODE packet.
 *
 */
static BOOL testModeResponse(void)
{
  //Note we are always running in this mode
  return bTRUE;
}

/*! @brief Responds to a TARIFF packet.
 *
 */
static BOOL tariffResponse(void)
{
  if(Packet_Parameter1 > 0 && Packet_Parameter1 < 4)
  {
    return (Tariff_SetTariff(Packet_Parameter1));
  }

  return bFALSE;
}

/*! @brief Responds to a TIME1 packet.
 *
 */
static BOOL time1Response(void)
{
  uint8_t days, hours, minutes, seconds;
  BOOL responseOk = bFALSE;

  RTC_CalcTime(Meter_Time, &days, &hours, &minutes, &seconds);

  if (Packet_Parameter3 == 1) //User wishes to read seconds and minutes
  {
    responseOk = Packet_Put(TIME1_COMMAND, seconds, minutes, 0);
  }
  else if (Packet_Parameter3 == 2) //User wishes to set seconds and minutes
  {
    if (Packet_Parameter1 >= 0 && Packet_Parameter1 < 60)
    {
      if (Packet_Parameter2 >= 0 && Packet_Parameter2 < 60)
      {
        Meter_Time = Packet_Parameter1 + (Packet_Parameter2 * 60) + (hours * 3600) + (days * 86400);
        responseOk = bTRUE;
      }
    }
  }

  return (responseOk);
}

/*! @brief Responds to a TIME2 packet.
 *
 */
static BOOL time2Response(void)
{
  uint8_t days, hours, minutes, seconds;
  BOOL responseOk = bFALSE;

  RTC_CalcTime(Meter_Time, &days, &hours, &minutes, &seconds);

  if (Packet_Parameter3 == 1) //User wishes to read hours and days
  {
    responseOk = Packet_Put(TIME1_COMMAND, seconds, minutes, 0);
  }
  else if (Packet_Parameter3 == 2) //User wishes to set hours and days
  {
    if (Packet_Parameter1 >= 0 && Packet_Parameter1 < 24)
    {
      if (Packet_Parameter2 >= 0 && Packet_Parameter2 < 100)
      {
        Meter_Time = seconds + (minutes * 60) + (Packet_Parameter1 * 3600) + (Packet_Parameter2 * 86400);
        responseOk = bTRUE;
      }
    }
  }

  return (responseOk);
}

/*! @brief Responds to a POWER packet.
 *
 */
static BOOL powerResponse(void)
{
  uint16_t powerInteger, powerFraction;
  uint16union_t power;

  normalisePower(&powerInteger, &powerFraction, bFALSE);
  power.l = powerInteger;

  return Packet_Put(POWER_COMMAND, power.s.Lo, power.s.Hi, 0);
}

/*! @brief Responds to a ENERGY packet.
 *
 */
static BOOL energyResponse(void)
{
  uint16union_t energy;
  uint16_t energyWhole, energyFraction;

  normaliseEnergy(&energyWhole, &energyFraction);
  energy.l = (energyWhole*1000) + energyFraction;  //Apply the extra scaling factor of 1000 for displaying energy in a packet

  return (Packet_Put(ENERGY_COMMAND, energy.s.Lo, energy.s.Hi, 0));
}

/*! @brief Responds to a COST packet.
 *
 */
static BOOL costResponse(void)
{
  uint16_t dollars;
  uint8_t cents;
  uint16union_t dollarsUnion;

  normaliseCost(&dollars, &cents);
  dollarsUnion.l = dollars;

  return (Packet_Put(ENERGY_COMMAND, cents, dollarsUnion.s.Lo, dollarsUnion.s.Hi));
}

/*! @brief Responds to a FREQUENCY packet.
 *
 */
static BOOL frequencyResponse(void)
{
  //Waveforms are stuck at 50 Hz
  return (Packet_Put(FREQUENCY_COMMAND, 50, 0, 0));
}

/*! @brief Responds to a VOLT_RMS packet.
 *
 */
static BOOL voltRMSResponse(void)
{
  uint16union_t voltageRMS;
  voltageRMS.l = Meter_VoltageRMS;

  return (Packet_Put(VOLT_RMS_COMMAND, voltageRMS.s.Lo, voltageRMS.s.Hi, 0));
}

/*! @brief Responds to a CURR_RMS packet.
 *
 */
static BOOL currRMSResponse(void)
{
  uint16union_t currentRMS;
  currentRMS.l = Meter_CurrentRMS;

  return (Packet_Put(CURR_RMS_COMMAND, currentRMS.s.Lo, currentRMS.s.Hi, 0));
}

/*! @brief Responds to a PFACTOR packet.
 *
 */
static BOOL powerFactorResponse(void)
{
  uint16union_t powerFactor;
  uint32_t pf = DAC_PowerFactor();

  powerFactor.l = (uint16_t)((pf*1000) >> 16); //Scale up power factor by 1000

  return (Packet_Put(PFACTOR_COMMAND, powerFactor.s.Lo, powerFactor.s.Hi, 0));
}

/*! @brief Responds to a VOLTAGE_AMP packet.
 *
 */
static BOOL voltageAmplitudeResponse(void)
{
  int16union_t voltageAmplitude;

  voltageAmplitude.s.Lo = Packet_Parameter1;
  voltageAmplitude.s.Hi = Packet_Parameter2;

  if (voltageAmplitude.l >= VOLTAGE_MIN_VALUE && voltageAmplitude.l <= VOLTAGE_MAX_VALUE)
  {
    DAC_SetVoltageAmplitude(voltageAmplitude.l);
    return bTRUE;
  }

  return bFALSE;
}

/*! @brief Responds to a CURRENT_AMP packet.
 *
 */
static BOOL currentAmplitudeResponse(void)
{
  int16union_t currentAmplitude;

  currentAmplitude.s.Lo = Packet_Parameter1;
  currentAmplitude.s.Hi = Packet_Parameter2;

  if (currentAmplitude.l >= CURRENT_MIN_VALUE && currentAmplitude.l <= CURRENT_MAX_VALUE)
  {
    DAC_SetCurrentAmplitude(currentAmplitude.l);
    return bTRUE;
  }

  return bFALSE;
}

/*! @brief Responds to a PHASE packet.
 *
 */
static BOOL phaseResponse(void)
{
  if (Packet_Parameter1 >= 0 && Packet_Parameter1 <= 32)
  {
    /* Phase is represented as a series of steps:
     * PhaseStep = 0 -> -90 degrees
     * PhaseStep = 32 -> 90 degrees
     */
    DAC_SetPhaseDifference(Packet_Parameter1);
    return bTRUE;
  }

  return bFALSE;
}

/*! @brief This will decide how to respond to an incoming packet from the PC
 *
 *  @return void
 */
static void packetHandle(void)
{
  //We copy this variable so as to not modify the original when we are removing the ACK bit
  uint8_t command = Packet_Command;
  BOOL responseOk = bFALSE;

  //If Packet_Get is successful in receiving a packet from the PC then parse the command in packet
  if (Packet_Get())
  {
    //Remove the ACK bit to get the command in 'base' form
    command &= ~(PACKET_ACK_MASK);

    switch (command)
    {
      case TEST_MODE_COMMAND:
        responseOk = testModeResponse();
        break;
      case TARIFF_COMMAND:
        responseOk = tariffResponse();
        break;
      case TIME1_COMMAND:
        responseOk = time1Response();
        break;
      case TIME2_COMMAND:
        responseOk = time2Response();
        break;
      case POWER_COMMAND:
        responseOk = powerResponse();
        break;
      case ENERGY_COMMAND:
        responseOk = energyResponse();
        break;
      case COST_COMMAND:
        responseOk = costResponse();
        break;
      case FREQUENCY_COMMAND:
        responseOk = frequencyResponse();
        break;
      case VOLT_RMS_COMMAND:
        responseOk = voltRMSResponse();
        break;
      case CURR_RMS_COMMAND:
        responseOk = currRMSResponse();
        break;
      case PFACTOR_COMMAND:
        responseOk = powerFactorResponse();
        break;
      case VOLT_AMP_COMMAND:
        responseOk = voltageAmplitudeResponse();
        break;
      case CURR_AMP_COMMAND:
        responseOk = currentAmplitudeResponse();
        break;
      case PHASE_COMMAND:
        responseOk = phaseResponse();
        break;
    }

    //Check if ACK is required
    if (CHECK_BIT_SET(Packet_Command, 7))
    {
      //In this case, an additional ACK packet is required to be sent if the PC requires an acknowledgment
      if (responseOk)
      {
        command |= SET_BIT(7);
      }

      Packet_Put(command, Packet_Parameter1, Packet_Parameter2, Packet_Parameter3);
    }
  }
}

/*! @brief Initializes pin information for switch 1.
 *
 *  @return BOOL - TRUE if init was okay
 */
static BOOL switch1Init(void)
{
  Switch1Pressed = OS_SemaphoreCreate(0); //Initialize semaphore relating to the switch

  //Clock Gating
  SIM_SCGC5  |= SIM_SCGC5_PORTD_MASK;

  //Set type to GPIO - The pins are set as input by default
  PORTD_PCR0 &= ~PORT_PCR_MUX_MASK;
  PORTD_PCR0 |= PORT_PCR_MUX(1);

  //Setup interrupts for Pin
  PORTD_PCR0 |= PORT_PCR_ISF_MASK;
  PORTD_PCR0 |= PORT_PCR_IRQC(10);

  //Set Pull-up Resistors
  PORTD_PCR0 |= PORT_PCR_PE_MASK;
  PORTD_PCR0 |= PORT_PCR_PS_MASK;

  //Initialize NVIC
  NVICICPR2  |= (1 << 26); //PORTD
  NVICISER2  |= (1 << 26);

  return (bTRUE);
}

BOOL Interface_Init(void)
{
  BOOL modulesOk = bFALSE;
  BOOL initOk    = bFALSE;

  //Initialize global variables
  DormantDisplayTimerCounter = 1;
  CurrentDisplay = DISPLAY_DORMANT;

  //Initialize relative semaphores
  PrintToTerminal = OS_SemaphoreCreate(0);

  //Initialize DAC (self-test waveforms) with 353.5 V and 7.072A and phase difference of 0
  DAC_Init(VOLTAGE_MAX_VALUE, CURRENT_MAX_VALUE, 16);

  //Initialize required modules
  modulesOk = Packet_Init(BAUD_RATE, CPU_BUS_CLK_HZ) && FTM_Init() &&
              RTC_Init(secondsCallBack, 0) && Debounce_Init(cycleDisplayCallBack, 0) && switch1Init();

  if (modulesOk)
  {
    initOk = FTM_Set(&DISPLAY_DORMANT_TIMER);
  }

  return (initOk);
}

void Interface_PrintData(void *pData)
{
  TDisplayState display;
  for (;;)
  {
    OS_SemaphoreWait(PrintToTerminal, 0);

    OS_DisableInterrupts();
    display = CurrentDisplay;
    OS_EnableInterrupts();

    switch (display)
    {
      case DISPLAY_METER_TIME:
        printTime();
        break;
      case DISPLAY_AVERAGE_POWER:
        printPower();
        break;
      case DISPLAY_TOTAL_ENERGY:
        printEnergy();
        break;
      case DISPLAY_TOTAL_COST:
        printCost();
        break;
    }
  }
}

void Interface_PacketHandle(void *pData)
{
  for (;;)
  {
    packetHandle(); //Handle Packet Data
  }
}

void Interface_CycleDisplay(void *pData)
{
  for (;;)
  {
    OS_SemaphoreWait(Switch1Pressed, 0);

    OS_DisableInterrupts();               //Enter Critical section of Code
    DormantDisplayTimerCounter = 1;       //Reset the TimeoutCounter

    //Advance display state machine
    switch (CurrentDisplay)
    {
      case DISPLAY_DORMANT:
        CurrentDisplay = DISPLAY_METER_TIME;
        break;
      case DISPLAY_METER_TIME:
        CurrentDisplay = DISPLAY_AVERAGE_POWER;
        break;
      case DISPLAY_AVERAGE_POWER:
        CurrentDisplay = DISPLAY_TOTAL_ENERGY;
        break;
      case DISPLAY_TOTAL_ENERGY:
        CurrentDisplay = DISPLAY_TOTAL_COST;
        break;
      case DISPLAY_TOTAL_COST:
        CurrentDisplay = DISPLAY_METER_TIME;
        break;
    }

    FTM_StartTimer(&DISPLAY_DORMANT_TIMER);
    OS_EnableInterrupts();
  }
}

void __attribute__ ((interrupt)) SW1_ISR(void)
{
  PORTD_ISFR |= PORT_ISFR_ISF(0);       //Write 1 to clear the SW1 interrupt flag
  PORTD_PCR0 &= ~PORT_PCR_IRQC_MASK;    //Disable the interrupt on this pin so it no longer interrupts

  OS_ISREnter();
  Debounce_Start();
  OS_ISRExit();
}
/* END INTERFACE */
/*!
** @}
*/
