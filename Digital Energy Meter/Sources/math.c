/*! @file
 *
 *  @brief Library that performs math operations.
 *
 *  This module contains routines that allows a caller to perform mathematical operations.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup MATH_module MATH module documentation
**  @{
*/
/* MODULE MATH */
#include "math.h"

#define NB_ITERATIONS 25

uint32_t Math_SquareRoot(uint32_t num)
{
  uint8_t i;
  uint32_t lastGuess = 0;
  uint32_t estimatedValue = num / 2;

  //Using Newtonian method, find the root
  for (i = 0; i < NB_ITERATIONS; i++)
  {
    if (estimatedValue != 0)
    {
      estimatedValue = ((num / estimatedValue) + estimatedValue) / 2;

      if (estimatedValue == lastGuess)
        break;

      lastGuess = estimatedValue;
    }
    else
    {
      estimatedValue = 0;     //If we encounter a divide by zero error, break and return 0
      break;
    }
  }

  return (estimatedValue);
}
/* END MATH */
/*!
** @}
*/
