/*! @file
 *
 *  @brief Prints Meter statistics to the display.
 *
 *  This module contains routines that allows a caller print meter useage stats to the display on the PC interface.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup DISPLAY_PRINT_module Display Print module documentation
**  @{
*/
/* MODULE DISPLAY_PRINT */
#include "Cpu.h"
#include "displayprint.h"
#include "UART.h"
#include "OS.h"
#include <stdio.h>
#include <string.h>

void DisplayPrint_Time(uint8_t days, uint8_t hours, uint8_t minutes, uint8_t seconds)
{
  char buffer[14];

  if (days <= 99)
  {
    snprintf(buffer, 13, "%d:%d:%d:%d\n", days, hours, minutes, seconds);
  }
  else
  {
    snprintf(buffer, 13, "xx:xx:xx:xx\n");
  }

  UART_PrintDriver(buffer, strlen(buffer));
}

void DisplayPrint_Power(uint16_t whole, uint16_t fraction)
{
  char buffer[13];

  if (whole <= 999)
  {
    snprintf(buffer, 12, "%d.%03d kW\n", whole, fraction);
  }
  else
  {
    snprintf(buffer, 12, "xxx.xxx kW\n");
  }

  UART_PrintDriver(buffer, strlen(buffer));
}

void DisplayPrint_Energy(uint16_t whole, uint16_t fraction)
{
  char buffer[14];

  if (whole <= 999)
  {
    snprintf(buffer, 13, "%i.%03d kWh\n", whole, fraction);
  }
  else
  {
    snprintf(buffer, 13, "xxx.xxx kWh\n");
  }

  UART_PrintDriver(buffer, strlen(buffer));
}

void DisplayPrint_Cost(uint16_t dollars, uint8_t cents)
{
  char buffer[10];

  if (dollars <= 9999)
  {
    snprintf(buffer, 9, "$%d.%02d\n", dollars, cents);
  }
  else
  {
    snprintf(buffer, 9, "$xxxx.xx\n");
  }

  UART_PrintDriver(buffer, strlen(buffer));
}
/* END DISPLAY_PRINT */
/*!
** @}
*/

