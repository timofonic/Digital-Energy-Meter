/*! @file
 *
 *  @brief Digital to Analog Converter.
 *
 *  This module contains routines that allows a caller to obtain data information about a waveform.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup DAC_module DAC module documentation
**  @{
*/
/* MODULE DAC */
#include "DAC.h"

/*!< Simple LUT of a sine wave, ranging from angle 0 to 360 in 5.625 degree steps. The sampled values in 32Q16 format. */
const int32_t const SineWave[65] = {0, 6423, 12785, 19024, 25079, 30893, 36409, 41575, 46340, 50660, 54491, 57797,
                                    60547, 62714, 64276, 65220, 65536, 65220, 64276, 62714, 60547, 57797, 54491,
                                    50660, 46340, 41575, 36409, 30893, 25079, 19024, 12785, 6423, 0, -6423, -12785,
                                    -19024, -25079, -30893, -36409, -41575, -46340, -50660, -54491, -57797, -60547,
                                    -62714, -64276, -65220, -65536, -65220, -64276, -62714, -60547, -57797, -54491,
                                    -50660, -46340, -41575, -36409, -30893, -25079, -19024, -12785, -6423, 0};

typedef struct
{
  int16_t Amplitude;
}TWaveForm;

static TWaveForm VoltageWave;   /*!< Voltage Waveform. Amplitude base: 1000/2^15 */
static TWaveForm CurrentWave;   /*!< Current Waveform. Amplitude base: 10/2^15 */
static uint8_t PhaseDifference; /*!< The phase difference of the current waveform in reference to voltage. (Simply an integer offset within the array). */

BOOL DAC_Init(int16_t voltageAmp, int16_t currentAmp, uint16_t phaseStep)
{
  VoltageWave.Amplitude = voltageAmp;
  CurrentWave.Amplitude = currentAmp;
  DAC_SetPhaseDifference(phaseStep);

  return (bTRUE);
}

/*! @brief Will pick out a value from the Sine LUT.
 *
 *  @param offset   - Place to pick sine value out of array (0 - 64)
 *  @return int32_t - Value at LUT[offset]
 */
int32_t getValueFromLUT(uint16_t offset)
{
  uint16_t tempTheta;

  if (offset >= 0 && offset <= 64)
  {
    return SineWave[offset];
  }
  else
  {
    return 0;   //In the unlikely event that the user reads at an invalid offset, we just return 0.
  }
}

/*! @brief Get a sample from a 'self-test' waveform.
 *
 *  @param sample - A pointer to a variable to hold the sample.
 *  @param offset - Where to 'pick-off' the variable from the wave.
 *  @param waveform - The type of waveform
 */
void sampleWaveform(int16_t * sample, uint16_t offset, TWaveForm waveform)
{
  int64_t result = getValueFromLUT(offset) * waveform.Amplitude ;
  *sample = (int16_t)(result / 65536);  //Scale value back down to original base of waveform
}

uint32_t DAC_PowerFactor(void)
{
  return ((uint32_t) getValueFromLUT((16 + PhaseDifference) % 64));
}

void DAC_GetSample(int16_t * voltSample, int16_t * currSample, uint16_t voltPos, uint16_t currPos)
{
  //Ensure that voltPos and currPos are within the bounds of 64 size array
  voltPos = (voltPos % 64);
  currPos = ((currPos + PhaseDifference) % 64);  //Some phase difference may occur between the two waves (current in reference to voltage)

  sampleWaveform(voltSample, voltPos, VoltageWave);
  sampleWaveform(currSample, currPos, CurrentWave);
}

void DAC_SetVoltageAmplitude(int16_t voltageAmp)
{
  VoltageWave.Amplitude = voltageAmp;
}

void DAC_SetCurrentAmplitude(int16_t currentAmp)
{
  CurrentWave.Amplitude = currentAmp;
}

void DAC_SetPhaseDifference(uint8_t phaseStep)
{
  /* Phase is represented as a series of steps to the outside world
   * i.e. phaseStep = 0  -> -90 deg
   *      phaseStep = 16 -> 0 deg
   *      phaseStep = 24 -> 45 deg
   */
  PhaseDifference = (48 + phaseStep) % 64;
}
/* END DAC */
/*!
** @}
*/
