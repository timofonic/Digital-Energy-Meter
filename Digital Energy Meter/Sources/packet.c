/*! @file
 *
 *  @brief Routines to implement packet encoding and decoding for the serial port.
 *
 *  This contains the functions for implementing the "Tower to PC Protocol" 5-byte packets.
 *
 *  @author BAllen, APope
 *  @date 2015-08-15
 */
/*!
**  @addtogroup packet_module packet module documentation
**  @{
*/
/* MODULE packet */
#include "Cpu.h"
#include "UART.h"
#include "packet.h"
#include "OS.h"

TPacket Packet;

static uint8_t Checksum;	    /*!< The received packet's Checksum */
static uint8_t PacketState;	  /*!< Variable to keep track of the packet Finite State Machine */

static uint8_t calculatePacketChecksum(uint8_t command, uint8_t param1, uint8_t param2, uint8_t param3);

BOOL Packet_Init(const uint32_t baudRate, const uint32_t moduleClk)
{
  PacketState = 0;
  return (UART_Init(baudRate, moduleClk));
}

BOOL Packet_Get(void)
{
  uint8_t calculatedChecksum;	  //The calculated checksum from the command and 3 parameters of the packet
  uint8_t data; 		            //Used as a temporary variable to get data from RxFIFO

  if (PacketState != 5)
  {
    if (UART_InChar(&data))
    {
      switch (PacketState)
      {
        case 0:
          Packet_Command = data;
          PacketState++;
          break;
        case 1:
          Packet_Parameter1 = data;
          PacketState++;
          break;
        case 2:
          Packet_Parameter2 = data;
          PacketState++;
          break;
        case 3:
          Packet_Parameter3 = data;
          PacketState++;
          break;
        case 4:
          Checksum = data;
          PacketState++;
          break;
        default:
          break;
      }
    }
  }
  else if (PacketState == 5)
  {
    //Now we must validate the whole received packet
    calculatedChecksum = calculatePacketChecksum(Packet_Command, Packet_Parameter1, Packet_Parameter2, Packet_Parameter3);

    if (calculatedChecksum == Checksum)
    {
      PacketState = 0;
      return (bTRUE);
    }
    else
    {
      //Validation failed, shift the packet bytes to the left, and get a new one from RxFIFO the next time this function is called
      Packet_Command = Packet_Parameter1;
      Packet_Parameter1 = Packet_Parameter2;
      Packet_Parameter2 = Packet_Parameter3;
      Packet_Parameter3 = Checksum;

      PacketState--; //Move the Packet state back to state 4
    }
  }

  return (bFALSE);
}

/*! @brief Calculates the checksum of a packet by XOR'ing the preceding 4 bytes.
 *
 *  @param command The packet's command
 *  @param param1 The first parameter of the packet
 *  @param param2 The second parameter of the packet
 *  @param param3 The third parameter of the packet
 *  @return uint8_t - Packets calculated checksum.
 */
static uint8_t calculatePacketChecksum(uint8_t command, uint8_t param1, uint8_t param2, uint8_t param3)
{
  return (((command ^ param1) ^ param2) ^ param3); // XOR operation to determine checksum
}

BOOL Packet_Put(const uint8_t command, const uint8_t parameter1, const uint8_t parameter2, const uint8_t parameter3)
{
  uint8_t checksum = calculatePacketChecksum(command, parameter1, parameter2, parameter3); //Calculate the checksum of the packet
  uint8_t buffer[] = {command, parameter1, parameter2, parameter3, checksum};

  return (UART_PrintDriver(buffer, 5));
}
/* END packet */
/*!
** @}
*/
