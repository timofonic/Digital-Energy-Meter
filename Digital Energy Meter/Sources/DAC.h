/*! @file
 *
 *  @brief Digital to Analog Converter.
 *
 *  This module contains routines that allows a caller to obtain data information about a waveform.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef DAC_H
#define DAC_H

#include "types.h"


/*! @brief Initializes the DAC before first use.
 *
 *  @param voltageAmp - Will set the Amplitude of the voltage waveform.
 *  @param currentAmp - Will set the Amplitude of the current waveform.
 *  @param phaseStep  - The phase difference of the current waveform in reference to voltage. (0 - 32)
 *  @return BOOL      - TRUE if initialization was successful.
 *  @note Assumes voltageAmp & currentAmp have already been normalized to their respective 'bases'.
 */
BOOL DAC_Init(int16_t voltageAmp, int16_t currentAmp, uint16_t phaseStep);

/*! @brief Returns the power factor based on the phase difference.
 *
 *  @return uint32_t - power factor (base 2^16)
 */
uint32_t DAC_PowerFactor(void);

/*! @brief Obtains a voltage and current sample of corresponding 'waveforms'.
 *
 *  @param voltSample - Pointer to a variable to store the voltage sample
 *  @param currSample - Pointer to a variable to store the current sample
 *  @param voltPos    - Where to sample within the LUT.
 *  @param currPos    - Where to sample within the LUT.
 *  @return void
 */
void DAC_GetSample(int16_t * voltSample, int16_t * currSample, uint16_t voltPos, uint16_t currPos);

/*! @brief Sets the amplitude of the voltage waveform.
 *
 *  @param voltageAmp - The amplitude to set. (32Q16)
 *  @return void
 */
void DAC_SetVoltageAmplitude(int16_t voltageAmp);

/*! @brief Sets the amplitude of the current waveform.
 *
 *  @param currentAmp - The amplitude to set. (32Q16)
 *  @return void
 */
void DAC_SetCurrentAmplitude(int16_t currentAmp);

/*! @brief Sets the phase difference between current in reference to voltage
 *
 *  @param phaseDifference - The phase step difference to set.
 *  @return void
 */
void DAC_SetPhaseDifference(uint8_t phaseStep);
#endif
