/*! @file
 *
 *  @brief Routines for controlling Periodic Interrupt Timer (PIT) on the TWR-K70F120M.
 *
 *  This contains the functions for operating the periodic interrupt timer (PIT).
 *
 *  @author BAllen, APope
 *  @date 2015-08-22
 */
/*!
**  @addtogroup PIT_module PIT module documentation
**  @{
*/
/* MODULE PIT */
#include "PIT.h"
#include "MK70F12.h"
#include "bits.h"
#include "OS.h"

static void (*ISRCallBack)(void *);	  /*!< The callback function that the PIT ISR will invoke */
static void *CallBackArguments;		    /*!< The arguments to pass to the ISRCallBack function */

static uint32_t ModuleClk;		        /*!< The PIT Module Clock in Hz */

BOOL PIT_Init(const uint32_t moduleClk, void (*userFunction)(void *), void *userArguments)
{
  ISRCallBack = userFunction;
  CallBackArguments = userArguments;
  
  //Enable clock gating for the PIT module
  SIM_SCGC6 |= SIM_SCGC6_PIT_MASK;

  //Set up the Module Control Register
  PIT_MCR |= PIT_MCR_MDIS_MASK;		//Disable the PIT Module initially
  PIT_MCR |= PIT_MCR_FRZ_MASK; 		//Freeze timer during debug

  //Set the PIT module clock
  ModuleClk = moduleClk;

  NVICICPR2 = (1 << 4); 		//Clear pending interrupts for PIT in NVIC
  NVICISER2 = (1 << 4); 		//Enable interrupts from PIT module in NVIC

  PIT_MCR &= ~PIT_MCR_MDIS_MASK; 	//Enable the PIT module
  return (bTRUE);
}

void PIT_Set(const uint32_t period, const BOOL restart)
{
  PIT_LDVAL0 = (((ModuleClk)/(1000000000/period)) - 1); //The amount of 'ticks' for the desired period

  if (restart)
  {
    PIT_Enable(bFALSE);	//Disable the PIT Timer
    PIT_Enable(bTRUE);	//Re-enable the Timer with the new value of LDVAL
  }

  //Enable PIT Interrupts
  PIT_TCTRL0 |= PIT_TCTRL_TIE_MASK;
}

void PIT_Enable(const BOOL enable)
{
  if (enable)
  {
    PIT_TCTRL0 |= PIT_TCTRL_TEN_MASK;
  }
  else
  {
    PIT_TCTRL0 &= ~PIT_TCTRL_TEN_MASK;
  }
}

void __attribute__ ((interrupt)) PIT_ISR(void)
{
  PIT_TFLG0 |= PIT_TFLG_TIF_MASK; //Clear the Interrupt Flag

  OS_ISREnter();
  ISRCallBack(CallBackArguments); //Invoke the Call-back function
  OS_ISRExit();
}
/* END PIT */
/*!
** @}
*/
