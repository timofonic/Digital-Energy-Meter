/*! @file
 *
 *  @brief Routines for erasing and writing to the Flash.
 *
 *  This contains the functions needed for accessing the internal Flash.
 *
 *  @author BAllen, APope
 *  @date 2015-08-14
 */
/*!
**  @addtogroup Flash_module Flash module documentation
**  @{
*/
/* MODULE FLASH */
#include "Cpu.h"
#include "MK70F12.h"
#include "Flash.h"

#define ADDRBYTEMASK       0x0000000F		//Used to get obtain the LSByte of an Address
#define ERASE_SECTOR_CMD   0x09			    //The 'Erase Sector' Flash command
#define PROGRAM_PHRASE_CMD 0x07			    //The 'Program Phrase' Flash command

// A structure which is used to initialize the flash command registers for a particular write operation.
/*!
 * @struct Flash_Command
 */
typedef struct
{
  uint8_t FCMD;			      /*!< The flash command code */
  uint8_t AddressByte2; 	/*!< Flash address [23:16] */
  uint8_t AddressByte1;		/*!< Flash address [15:8] */
  uint8_t AddressByte0;		/*!< Flash address [7:0] */
  uint8_t Data[8];		    /*!< Flash Data */
} Flash_Command;

//An array of address locations that represents Flash Block 2, Sector 0, phrase 0
unsigned char volatile * const Flash_AddrRange[8] = {
    FLASHMEMADDR0,
    FLASHMEMADDR1,
    FLASHMEMADDR2,
    FLASHMEMADDR3,
    FLASHMEMADDR4,
    FLASHMEMADDR5,
    FLASHMEMADDR6,
    FLASHMEMADDR7,
};

//An array of flash address locations available for allocation
static unsigned char volatile * FreeFlashAddr[8];

static BOOL initCommandRegisters(Flash_Command * commandRegisters, uint8_t FCMD);
static void flashRead(Flash_Command * commandRegisters);
static BOOL flashCommandExecute(Flash_Command * const commandRegisters);

/*! @brief Enables the Flash module.
 *
 *  @return BOOL - TRUE if the Flash was setup successfully.
 */
BOOL Flash_Init(void)
{
  //Enable clock gating for flash memory
  SIM_SCGC3 |= SIM_SCGC3_NFC_MASK;

  //Initialize the Flash memory allocation array
  FreeFlashAddr[0] = FLASHMEMADDR0;
  FreeFlashAddr[1] = FLASHMEMADDR1;
  FreeFlashAddr[2] = FLASHMEMADDR2;
  FreeFlashAddr[3] = FLASHMEMADDR3;
  FreeFlashAddr[4] = FLASHMEMADDR4;
  FreeFlashAddr[5] = FLASHMEMADDR5;
  FreeFlashAddr[6] = FLASHMEMADDR6;
  FreeFlashAddr[7] = FLASHMEMADDR7;

  return (bTRUE);
}

/*! @brief Allocates space for a non-volatile variable in the Flash memory.
 *
 *  @param variable is the address of a pointer to a variable that is to be allocated space in Flash memory.
 *  @param size The size, in bytes, of the variable that is to be allocated space in the Flash memory. Valid values are 1, 2 and 4.
 *  @return BOOL - TRUE if the variable was allocated space in the Flash memory.
 *  @note Assumes Flash has been initialized.
 */
BOOL Flash_AllocateVar(volatile void **variable, const uint8_t size)
{
  int i;
  BOOL spaceAllocated = bFALSE;

  EnterCritical();

  /* In this code, depending on the size of the variable, we will loop through the AvailableFlashAddress
   * array, and check to see if that flash Address is available for assignment. An array element of '0'
   * indicates that that address has already been taken.
   *
   * We can safely assume that the user will never want to obtain the address '0x0000000', as we know it is
   * not part of our flash address range for our micro-controller.
   */
  if (size == 1 || size == 2 || size == 4)
  {
    for (i = 0; i < sizeof(FreeFlashAddr); i+=size)
    {
      if (FreeFlashAddr[i] != 0)
      {
        if (size == 1)
        {
          *variable = FreeFlashAddr[i]; 	//Assign the address to the variable
          FreeFlashAddr[i] = 0; 		//Clear this element to indicate that the address is no longer available
          spaceAllocated = bTRUE;
          break;
        }
        else if (size == 2)
        {
          if (FreeFlashAddr[i+1] != 0) //Two spaces are required to store this data
          {
            *variable = FreeFlashAddr[i];
            FreeFlashAddr[i] = 0;
            FreeFlashAddr[i+1] = 0;
            spaceAllocated = bTRUE;
            break;
          }
        }
        else if (size == 4)
        {
          //Four spaces are required to store this data
          if (FreeFlashAddr[i+1] != 0 && FreeFlashAddr[i+2] != 0 && FreeFlashAddr[i+3] != 0)
          {
            *variable = FreeFlashAddr[i];

            FreeFlashAddr[i] = 0;
            FreeFlashAddr[i+1] = 0;
            FreeFlashAddr[i+2] = 0;
            FreeFlashAddr[i+3] = 0;
            spaceAllocated = bTRUE;
            break;
          }
        }
      }
    }
  }

  ExitCritical();

  return (spaceAllocated);
}

/*! @brief Writes a 32-bit number to Flash.
 *
 *  @param address The address of the data.
 *  @param data The 32-bit data to write.
 *  @return BOOL - TRUE if Flash was written successfully, FALSE if address is not aligned to a 4-byte boundary or if there is a programming error.
 *  @note Assumes Flash has been initialized.
 */
BOOL Flash_Write32(uint32_t volatile * const address, const uint32_t data)
{
  Flash_Command flashWrite;
  uint8_t allignedByte;

  //Variables to store parts of data
  uint32union_t dataLongWord;
  uint16union_t dataWord0, dataWord1;
  uint8_t byte0, byte1, byte2, byte3;

  //Splitting 'data' into easy accessible bytes
  dataLongWord.l = data;
  dataWord0.l = dataLongWord.s.Hi; dataWord1.l = dataLongWord.s.Lo;
  byte0 = dataWord0.s.Hi;
  byte1 = dataWord0.s.Lo;
  byte2 = dataWord1.s.Hi;
  byte3 = dataWord1.s.Lo;

  if (initCommandRegisters(&flashWrite, PROGRAM_PHRASE_CMD)) //Populate the registers with address and data information
  {
    if (((uint32_t) address % 4) == 0)
    {
      /* We want to be able to determine what byte this address is aligned upon, and therefore determine
       * which bytes of the phrase we are wishing to modify.
       */
      allignedByte = ((uint32_t) address & ADDRBYTEMASK);
      allignedByte = (allignedByte % 8);

      //Modify the data registers accordingly
      flashWrite.Data[allignedByte] 	= byte0;
      flashWrite.Data[(allignedByte+1)] = byte1;
      flashWrite.Data[(allignedByte+2)] = byte2;
      flashWrite.Data[(allignedByte+3)] = byte3;

      if (Flash_Erase())
      {
        return (flashCommandExecute(&flashWrite));
      }
    }
  }

  return (bFALSE);
}

/*! @brief Writes a 16-bit number to Flash.
 *
 *  @param address The address of the data.
 *  @param data The 16-bit data to write.
 *  @return BOOL - TRUE if Flash was written successfully, FALSE if address is not aligned to a 2-byte boundary or if there is a programming error.
 *  @note Assumes Flash has been initialized.
 */
BOOL Flash_Write16(uint16_t volatile * const address, const uint16_t data)
{
  Flash_Command flashWrite;
  uint8_t allignedByte;

  //Variables to store parts of data
  uint16union_t dataWord;
  uint8_t byte0, byte1;

  //Splitting 'data' into easy accessible bytes
  dataWord.l = data;
  byte0 = dataWord.s.Hi;
  byte1 = dataWord.s.Lo;

  if (initCommandRegisters(&flashWrite, PROGRAM_PHRASE_CMD)) //Populate the FFCOB registers with address and data information
  {
    if (((uint32_t) address % 2) == 0)
    {
      /* We want to be able to determine what byte this address is aligned upon, and therefore determine
       * which bytes of the phrase we are wishing to modify.
       */
      allignedByte = ((uint32_t) address & ADDRBYTEMASK);
      allignedByte = (allignedByte % 8);

      //Modify the data registers accordingly
      flashWrite.Data[allignedByte] 	= byte0;
      flashWrite.Data[(allignedByte+1)] = byte1;

      if (Flash_Erase())
      {
        return (flashCommandExecute(&flashWrite));
      }
    }
  }

  return (bFALSE);
}

/*! @brief Writes an 8-bit number to Flash.
 *
 *  @param address The address of the data.
 *  @param data The 8-bit data to write.
 *  @return BOOL - TRUE if Flash was written successfully, FALSE if there is a programming error.
 *  @note Assumes Flash has been initialized.
 */
BOOL Flash_Write8(uint8_t volatile * const address, const uint8_t data)
{
  Flash_Command flashWrite;
  uint8_t allignedByte;

  if (initCommandRegisters(&flashWrite, PROGRAM_PHRASE_CMD))	//Populate the FFCOB with address and current data information
  {
    /* We want to be able to determine what byte this address is aligned upon, and therefore determine
     * which bytes of the phrase we are wishing to modify.
     */
    allignedByte = ((uint32_t) address & ADDRBYTEMASK);
    allignedByte = (allignedByte % 8);

    //Modify the desired byte of the data accordingly
    flashWrite.Data[allignedByte] = data;

    if (Flash_Erase())
    {
      return (flashCommandExecute(&flashWrite));
    }
  }

  return (bFALSE);
}

/*! @brief Erases the entire Flash sector.
 *
 *  @return BOOL - TRUE if the Flash "data" sector was erased successfully.
 *  @note Assumes Flash has been initialized.
 */
BOOL Flash_Erase(void)
{
  Flash_Command flashErase;

  if (initCommandRegisters(&flashErase, ERASE_SECTOR_CMD)) //Populate the registers with address information
  {
    return (flashCommandExecute(&flashErase));
  }

  return (bFALSE);
}

/*! @brief Populates a flash command struct with address and FCMD information.
 *
 *  @param commandRegisters The Flash_Command struct to populate.
 *  @param FCMD The flash command code.
 *  @return BOOL - TRUE if Struct populated, FALSE if unrecognized FCMD.
 *  @note Assumes Flash has been initialized.
 */
static BOOL initCommandRegisters(Flash_Command * commandRegisters, uint8_t FCMD)
{
  //In our micro-controller we are limited to the flash address range 0x0008_0000 to 0x0008_0007
  //Therefore we know we can initialize the FFCOB registers containing address information to:
  commandRegisters->AddressByte2 = 0x08;
  commandRegisters->AddressByte1 = 0x00;
  commandRegisters->AddressByte0 = 0x00;

  switch (FCMD)
  {
    case ERASE_SECTOR_CMD:				//Flash 'Erase Sector'
      commandRegisters->FCMD = FCMD;
      break;
    case PROGRAM_PHRASE_CMD:				//Flash 'Program Phrase'
      commandRegisters->FCMD = FCMD;
      flashRead(commandRegisters); //Store the current contents of flash memory into the data section of the FFCOB registers
      break;
    default:
      return (bFALSE);
      break;
  }

  return (bTRUE);
}

/*! @brief Populates a flash command struct with the current contents of flash memory.
 *
 *  @param commandRegisters The Flash_Command struct to populate with data information.
 *  @return void
 */
static void flashRead(Flash_Command * commandRegisters)
{
  int i;
  for (i = 0; i < sizeof(commandRegisters); i++)
  {
    commandRegisters->Data[i] = *(Flash_AddrRange[i]);
  }
}

/*! @brief Executes a flash command operation in hardware.
 *
 *  @param commandRegisters The desired flash command to execute in hardware.
 *  @return BOOL - TRUE if operation completed successfully.
 *  @note Assumes Flash has been initialized.
 */
static BOOL flashCommandExecute(Flash_Command * const commandRegisters)
{
  while (!(FTFE_FSTAT & FTFE_FSTAT_CCIF_MASK)); //Waiting for the previous flash command (if any) to complete

  //Clear any erroneous error bits it may have set
  FTFE_FSTAT = FTFE_FSTAT_ACCERR_MASK | FTFE_FSTAT_FPVIOL_MASK | FTFE_FSTAT_RDCOLERR_MASK;

  FTFE_FCCOB0 = commandRegisters->FCMD;		      //Code that defines the FTFE command

  FTFE_FCCOB1 = commandRegisters->AddressByte2;	//FLash Address [23:16]
  FTFE_FCCOB2 = commandRegisters->AddressByte1;	//FLash Address [15:8]
  FTFE_FCCOB3 = commandRegisters->AddressByte0;	//FLash Address [7:0]

  FTFE_FCCOB4 = commandRegisters->Data[3];	    //The data is written using a big endian convention
  FTFE_FCCOB5 = commandRegisters->Data[2];
  FTFE_FCCOB6 = commandRegisters->Data[1];
  FTFE_FCCOB7 = commandRegisters->Data[0];
  FTFE_FCCOB8 = commandRegisters->Data[7];
  FTFE_FCCOB9 = commandRegisters->Data[6];
  FTFE_FCCOBA = commandRegisters->Data[5];
  FTFE_FCCOBB = commandRegisters->Data[4];

  FTFE_FSTAT = FTFE_FSTAT_CCIF_MASK;			          //Set the CCIF Flag indicating that we are ready to execute
  while (!(FTFE_FSTAT & FTFE_FSTAT_CCIF_MASK)); 	  //Wait for hardware to execute the command

  return (!(FTFE_FSTAT & FTFE_FSTAT_MGSTAT0_MASK));	//Return bFALSE for any errors it may have caused
}
/* END FLASH */
/*!
** @}
*/
