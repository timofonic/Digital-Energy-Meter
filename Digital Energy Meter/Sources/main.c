/* ###################################################################
**     Filename    : main.c
**     Project     : Digital Energy Meter
**     Processor   : MK70FN1M0VMJ12
**     Version     : Driver 01.01
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-07-20, 13:27, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.0
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */
#include "Cpu.h"
#include "Events.h"
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "meter.h"
#include "interface.h"
#include "OS.h"

#define THREAD_STACK_SIZE 400

static uint32_t InterfacePacketStack[THREAD_STACK_SIZE] __attribute__ ((aligned(0x08)));
static uint32_t InterfacePrintStack[THREAD_STACK_SIZE]  __attribute__ ((aligned(0x08)));
static uint32_t InterfaceCycleStack[THREAD_STACK_SIZE]  __attribute__ ((aligned(0x08)));
static uint32_t MeterCalcStack[THREAD_STACK_SIZE]       __attribute__ ((aligned(0x08)));
static uint32_t MeterSampleStack[THREAD_STACK_SIZE]     __attribute__ ((aligned(0x08)));

/*lint -save  -e970 Disable MISRA rule (6.3) checking. */
int main(void)
/*lint -restore Enable MISRA rule (6.3) checking. */
{
  BOOL initOk = bFALSE;
  OS_ERROR error;
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  OS_DisableInterrupts();

  initOk = Interface_Init() && Meter_Init();  //Initialize the DEM
  OS_Init(CPU_CORE_CLK_HZ);                   //Initialize the RTOS

  //Create Threads
  OS_ThreadCreate(Interface_PacketHandle,
                  NULL,
                  &InterfacePacketStack[THREAD_STACK_SIZE - 1],
                  4);

  OS_ThreadCreate(Interface_CycleDisplay,
                  NULL,
                  &InterfaceCycleStack[THREAD_STACK_SIZE - 1],
                  3);

  OS_ThreadCreate(Interface_PrintData,
                  NULL,
                  &InterfacePrintStack[THREAD_STACK_SIZE - 1],
                  2);

  OS_ThreadCreate(Meter_RunCalculations,
                  NULL,
                  &MeterCalcStack[THREAD_STACK_SIZE - 1],
                  1);

  OS_ThreadCreate(Meter_GetSamples,
                  NULL,
                  &MeterSampleStack[THREAD_STACK_SIZE - 1],
                  0);

  if (initOk)
  {
    OS_Start();   //Start Multi-threading!
  }

  OS_EnableInterrupts();
  for(;;){}
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/
/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
