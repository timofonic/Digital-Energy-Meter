/*! @file
 *
 *  @brief Routines for setting up the flexible timer module (FTM) on the TWR-K70F120M.
 *
 *  This contains the functions for operating the flexible timer module (FTM).
 *
 *  @author BAllen, APope
 *  @date 2015-09-04
 */
/*!
**  @addtogroup FTM_module FTM module documentation
**  @{
*/
/* MODULE FTM */
#include "MK70F12.h"
#include "FTM.h"
#include "OS.h"

#define CLOCK_SRC_NONE 		    0
#define CLOCK_SRC_SYSTEM 	    1
#define CLOCK_SRC_FIXED_FREQ 	2
#define CLOCK_SRC_EXTERNAL 	  3

#define PRESCALE_DIVIDE_1	    0
#define PRESCALE_DIVIDE_2	    1
#define PRESCALE_DIVIDE_4	    2
#define PRESCALE_DIVIDE_8	    3
#define PRESCALE_DIVIDE_16	  4
#define PRESCALE_DIVIDE_32	  5
#define PRESCALE_DIVIDE_64	  6
#define PRESCALE_DIVIDE_128	  7

#define NUM_FTM_CHANNELS	    8

/*!
 * @struct ChannelCallBack
 */
typedef struct
{
  TTimerFunction timerFunction;		/*!< Select function of timer channel (Input Capture | Output Compare)*/
  void (*userFunction)(void *);		/*!< Callback function to run in timer ISR*/
  void *userArguments;			      /*!< Arguments to pass into callback function*/
} ChannelCallBack;

static ChannelCallBack FTM0Channels[NUM_FTM_CHANNELS];	/*!< Table of callback information for each channel */

BOOL FTM_Init()
{
  //Enable Clock gating
  SIM_SCGC6 |= SIM_SCGC6_FTM0_MASK;

  //Initially load the Modulo and Count Registers
  FTM0_CNTIN = 0;
  FTM0_MOD = 0xFFFF;
  FTM0_CNT = 0;

  //Set-up Status and Control register
  FTM0_SC &= ~FTM_SC_TOIE_MASK; 	              //Timer Overflow Interrupt Enable: 0 disabled
  FTM0_SC &= ~FTM_SC_CPWMS_MASK;	              //Center-Aligned PWM Select: 0 Up counting mode

  FTM0_SC &= ~FTM_SC_CLKS_MASK;		              //Clock Source Selection
  FTM0_SC |= FTM_SC_CLKS(CLOCK_SRC_FIXED_FREQ);
  FTM0_SC &= ~FTM_SC_PS_MASK;		                //Pre-scale Factor Selection:
  FTM0_SC |= FTM_SC_PS(PRESCALE_DIVIDE_1);

  //Init NVIC
  NVICICPR1 = (1 << 30);
  NVICISER1 = (1 << 30);

  FTM0_MODE |= FTM_MODE_FTMEN_MASK;

  return (bTRUE);
}

BOOL FTM_Set(const TFTMChannel* const aFTMChannel)
{
  //Check that the channel is within the known range (0 - 7)
  if (aFTMChannel->channelNb >= 0 && aFTMChannel->channelNb < NUM_FTM_CHANNELS)
  {
    //Init the FTM0 Status & Control register for the particular channel
    switch (aFTMChannel->timerFunction)
    {
      case TIMER_FUNCTION_OUTPUT_COMPARE:
        FTM0_CnSC(aFTMChannel->channelNb) = ((aFTMChannel->ioType.outputAction << FTM_CnSC_ELSA_SHIFT) |
                                             (aFTMChannel->timerFunction << FTM_CnSC_MSA_SHIFT));
        break;
      case TIMER_FUNCTION_INPUT_CAPTURE:
        FTM0_CnSC(aFTMChannel->channelNb) = ((aFTMChannel->ioType.inputDetection << FTM_CnSC_ELSA_SHIFT) |
                                             (aFTMChannel->timerFunction << FTM_CnSC_MSA_SHIFT));
        break;
    }

    //Initialize the ChannelCallBack
    FTM0Channels[aFTMChannel->channelNb].timerFunction = aFTMChannel->timerFunction;
    FTM0Channels[aFTMChannel->channelNb].userFunction  = aFTMChannel->userFunction;
    FTM0Channels[aFTMChannel->channelNb].userArguments = aFTMChannel->userArguments;

    return(bTRUE);
  }

  return (bFALSE);
}

void FTM_StartTimer(const TFTMChannel* const aFTMChannel)
{
  switch (aFTMChannel->timerFunction)
  {
    case TIMER_FUNCTION_OUTPUT_COMPARE:
      FTM0_CnV(aFTMChannel->channelNb) = FTM0_CNT + aFTMChannel->delayCount;

      if (FTM0_CnSC(aFTMChannel->channelNb) & FTM_CnSC_CHF_MASK)
      {
        FTM0_CnSC(aFTMChannel->channelNb) &= ~FTM_CnSC_CHF_MASK;
      }

      break;
    case TIMER_FUNCTION_INPUT_CAPTURE:
      //Yet to Implement;
      //....
      break;
  }

  FTM0_CnSC(aFTMChannel->channelNb) |= FTM_CnSC_CHIE_MASK; //Enable Interrupts
}

void __attribute__ ((interrupt)) FTM0_ISR(void)
{
  int i;

  for (i = 0; i < 8; i++)
  {
    if ((FTM0_CnSC(i) & FTM_CnSC_CHF_MASK) && (FTM0_CnSC(i) & FTM_CnSC_CHIE_MASK))
    {
      if (FTM0Channels[i].timerFunction == TIMER_FUNCTION_OUTPUT_COMPARE)
      {
        FTM0_CnSC(i) &= ~FTM_CnSC_CHF_MASK;   //Clear the Interrupt Flag
        FTM0_CnSC(i) &= ~FTM_CnSC_CHIE_MASK;  //Stop any future interrupts from occurring (stop the timer)

        OS_ISREnter();
        FTM0Channels[i].userFunction(FTM0Channels[i].userArguments);  //Invoke the CallBackFunction
        OS_ISRExit();
      }
    }
  }
}
/* END FTM */
/*!
** @}
*/
