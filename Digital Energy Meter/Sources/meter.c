/*! @file
 *
 *  @brief Maintains usage statistics of the DEM.
 *
 *  This module contains the routines that monitors usage.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup METER_module METER module documentation
**  @{
*/
/* MODULE METER */
#include "sampler.h"
#include "math.h"
#include "meter.h"
#include "tariff.h"
#include "DAC.h"
#include "OS.h"

int64_t Meter_TotalEnergy;
int64_t Meter_TotalEnergyCost;
uint16_t Meter_PowerFactor;
uint16_t Meter_VoltageRMS;
uint16_t Meter_CurrentRMS;
uint32_t Meter_Time;

typedef enum {
  RMS_VOLTAGE,
  RMS_CURRENT
}TRMSType;

static int16_t VoltageSamples[16];      /*!< Contains the samples of the voltage waveform for a period. */
static int16_t CurrentSamples[16];      /*!< Contains the samples of the current waveform for a period. */
static BOOL PerformingCalculations;     /*!< Indicates that we are currently performing calculations on the two sample arrays. */

static OS_ECB *SamplesReady;            /*!< Semaphore to indicate when sample data from the 'sampler' module are ready to be read. */
static OS_ECB *RunCalculations;         /*!< Semaphore to indicate when the calculation routine should be run. */

/*! @brief Callback function that is invoked when a sample is taken from the self-test waveforms.
 *
 */
void sampleCallBack(void * nothing)
{
  OS_SemaphoreSignal(SamplesReady);     //Indicate that samples are ready to be read from the 'sampler' module.
}

/*! @brief Calculates RMS values for voltage and current.
 *
 *  @param sample             - Either a voltage or current sample (these values are accumulated within the function).
 *  @param allSamplesAquired  - Once all sample data has been acquired; this should be set to TRUE.
 *  @param rmsType            - An identifier for either calculating voltage or current RMS.
 */
static void calculateRMS(int16_t sample, BOOL allSamplesAquired, TRMSType rmsType)
{
  static uint32_t sumVoltageSamplesSquared = 0;
  static uint32_t sumCurrentSamplesSquared = 0;

  //Square the sample and accumulate
  uint32_t result = (sample * sample);
  switch (rmsType)
  {
    case RMS_VOLTAGE:
      sumVoltageSamplesSquared += (uint32_t)(((uint64_t)result*1000) >> 15); //Shift sample base back to original (2^15/1000)
      break;
    case RMS_CURRENT:
      sumCurrentSamplesSquared += (uint32_t)(((uint64_t)result*10) >> 15);   //Shift sample base back to original (2^15/10)
      break;
  }

  //By finding the sqrt(sum(samples^2)/16) we are able to find the RMS values for the corresponding waveform
  if (allSamplesAquired)
  {
    switch (rmsType)
    {
      case RMS_VOLTAGE:
        //Divide voltage sum by number of samples (always 16); Change 16 to same base -> 524 (2^15/1000)
        result = sumVoltageSamplesSquared / 524;
        Meter_VoltageRMS = (uint16_t) Math_SquareRoot(result);
        sumVoltageSamplesSquared = 0;
        break;
      case RMS_CURRENT:
        //Divide current sum by number of samples (always 16); Change 16 to same base -> 52428 (2^15/10)
        result = (((uint64_t)sumCurrentSamplesSquared * 1000000) / 52428);  //Scale up so result is in RMS mA
        Meter_CurrentRMS = (uint16_t) Math_SquareRoot(result);
        sumCurrentSamplesSquared = 0;
        break;
    }
  }
}

/*! @brief Calculates cost of energy for a particular period.
 *
 *  @param energyForPeriod - The energy calculated for one period.
 */
static void calculateCost(int32_t energyForPeriod)
{
  Meter_TotalEnergyCost += (int64_t) energyForPeriod * Tariff_GetTariffCost(Meter_Time);
}

/*! @brief Calculates energy over a period.
 *
 *  @param voltageSample - Instantaneous voltage sample
 *  @param currentSample - Instantaneous current sample
 *  @param allSamplesCollected - if TRUE, this means that this last sample was the final sample over the period (do extra calculations).
 */
static void calculateEnergy(int16_t voltageSample, int16_t currentSample, BOOL allSamplesCollected)
{
  static int32_t instantaneousPower = 0;
  instantaneousPower += (voltageSample * currentSample); //Accumulate instantaneous power over the period

  if (allSamplesCollected)
  {
    Meter_TotalEnergy += instantaneousPower;  //All samples are collected accumulate into total energy (Ts is accounted for in base)
    calculateCost(instantaneousPower);        //Calculate Cost for the period
    instantaneousPower = 0;
  }
}

/*! @brief Runs calculations on meter use statistics.
 *
 *  After all the samples over a period have been collected, this routine is invoked.
 */
static void runCalculations(void)
{
  uint8_t i;

  //Loop through the samples to -1 the total sample length
  for(i = 0; i < 15; i++)
  {
    calculateEnergy(VoltageSamples[i], CurrentSamples[i], bFALSE);
    calculateRMS(VoltageSamples[i], bFALSE, RMS_VOLTAGE);
    calculateRMS(CurrentSamples[i], bFALSE, RMS_CURRENT);
  }

  //Calculate the final values, indicating to the functions that all samples have been accounted for
  calculateEnergy(VoltageSamples[15], CurrentSamples[15], bTRUE);
  calculateRMS(VoltageSamples[15], bTRUE, RMS_VOLTAGE);
  calculateRMS(CurrentSamples[15], bTRUE, RMS_CURRENT);

  PerformingCalculations = bFALSE;
}

void Meter_IncrementTime(void)
{
  Meter_Time++;
}

BOOL Meter_Init(void)
{
  Meter_TotalEnergy     = 0;
  Meter_TotalEnergyCost = 0;
  Meter_PowerFactor     = 0;
  Meter_VoltageRMS      = 0;
  Meter_CurrentRMS      = 0;
  Meter_Time            = 0;
  PerformingCalculations   = bFALSE;

  SamplesReady      = OS_SemaphoreCreate(0);
  RunCalculations   = OS_SemaphoreCreate(0);

  return (Sampler_Init(sampleCallBack, 0) && Tariff_Init());
}

void Meter_GetSamples(void *pData)
{
  static uint8_t numberOfSamples = 0;

  for(;;)
  {
    OS_SemaphoreWait(SamplesReady, 0);  //Wait until the sampler module has sampled the data

    if (numberOfSamples != 16)
    {
      if (!PerformingCalculations)
      {
        //If we are not currently running calculations on these private global arrays, they are available for assignment
        VoltageSamples[numberOfSamples] = Sampler_VoltageSample;
        CurrentSamples[numberOfSamples] = Sampler_CurrentSample;
        numberOfSamples++;
      }
    }
    else
    {
      //We have collected all the required samples, signal other calculation thread to run its routines.
      numberOfSamples = 0;
      OS_SemaphoreSignal(RunCalculations);
    }
  }
}

void Meter_RunCalculations(void *pData)
{
  for(;;)
  {
    OS_SemaphoreWait(RunCalculations, 0);   //Wait until the sample arrays have been accordingly populated
    PerformingCalculations = bTRUE;
    runCalculations();
  }
}
/* END METER */
/*!
** @}
*/
