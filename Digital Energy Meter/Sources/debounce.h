/*! @file
 *
 *  @brief Routines for setting up the general purpose debounce module.
 *
 *  This contains functions for debouncing switches, pushbuttons, touch sensitive interfaces etc.
 *
 *  @author BAllen, APope
 *  @date 2015-09-04
 */

#ifndef DEBOUNCE_H
#define DEBOUNCE_H

// new types
#include "types.h"

typedef enum
{
  BUTTON_SW1,
  BUTTON_SW2,
} TDebounceID;

typedef struct
{
  TDebounceID buttonID;
  void (*debounceCompleteCallbackFunction)(void *);
  void* debounceCompleteCallbackArguments;
} TDebounce;

/*! @brief Sets up the debounce module before first use.
 *
 *  Enables FTM timers for the button debouncing.
 *
 *  @param userFunction - Is invoked after debounce and registering that button has been pressed.
 *  @param userArguments - UserFunction arguments
 *
 *  @return BOOL - TRUE if the debounce module was successfully initialized.
 */
BOOL Debounce_Init(void (*userFunction)(void *), void *userArguments);

/*! @brief Start debouncing switch 1.
 *
 *  Begins a 10ms delay to debounce switch 1.
 *
 *  @note Assumes Debounce_Init has been called
 */
void Debounce_Start(void);
#endif
