/*! @file
 *
 *  @brief Routines to access the LEDs on the TWR-K70F120M.
 *
 *  This contains the functions for operating the LEDs.
 *
 *  @author PMcL
 *  @date 2015-08-15
 */

#ifndef LEDS_H
#define LEDS_H

// new types
#include "types.h"

typedef enum
{
  LED_ORANGE = (1 << 11),
  LED_YELLOW = (1 << 28),
  LED_GREEN  = (1 << 29),
  LED_BLUE   = (1 << 10)
} TLED;

/*! @brief Sets up the LEDs before first use.
 *
 *  @return BOOL - TRUE if the LEDs were successfully initialized.
 */
BOOL LEDs_Init(void);

/*! @brief Turns an LED on.
 *
 *  @param colour The colour of the LED to turn on.
 *  @note Assumes that LEDs_Init has been called.
 */
void LEDs_On(const TLED colour);

/*! @brief Turns off an LED.
 *
 *  @param colour THe colour of the LED to turn off.
 *  @note Assumes that LEDs_Init has been called.
 */
void LEDs_Off(const TLED colour);

/*! @brief Toggles an LED.
 *
 *  @param colour THe colour of the LED to toggle.
 *  @note Assumes that LEDs_Init has been called.
 */
void LEDs_Toggle(const TLED colour);

/*! @brief Turns all the LEDs off
 *
 *  @note Assumes LEDs_Init has been called.
 */
void LEDs_AllOff(void);

/*! @brief Resets all the LEDs to their respective states just after a sucessfull Tower init.
 *
 *  @note Assumes that LEDs_Init has been called.
 */
void LEDs_ResetToTowerInit(void);
#endif
