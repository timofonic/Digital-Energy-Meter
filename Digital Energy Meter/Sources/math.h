/*! @file
 *
 *  @brief Library that performs math operations.
 *
 *  This module contains routines that allows a caller to perform mathematical operations.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef MATH_H
#define MATH_H

#include "types.h"

/*! @brief Calculates the square root of a number.
 *
 *  @param num - Positive unsigned integer to calculate square root value.
 *  @return int32_t - Result
 */
uint32_t Math_SquareRoot(uint32_t num);
#endif
