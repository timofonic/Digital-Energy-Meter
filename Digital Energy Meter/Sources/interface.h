/*! @file
 *
 *  @brief Acts as a human machine interface between the DEM and PC.
 *
 *  This module contains the routines that process user requests.
 *
 *  @author APope
 *  @date 2015-09-30
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include "types.h"

/*! @brief Initializes the Human machine interface before first use.
 *
 *  @return BOOL - TRUE if initialization was successful.
 */
BOOL Interface_Init(void);

/*! @brief Packet Handle Thread
 *
 *  @param pData - Generic variable to pass data into the Thread.
 *  @return void
 */
void Interface_PacketHandle(void *pData);

/*! @brief Print Data to the terminal Thread
 *
 *  @param pData - Generic variable to pass data into the Thread.
 *  @return void
 */
void Interface_PrintData(void *pData);

/*! @brief Cycle the display mode of the terminal Thread
 *
 *  @param pData - Generic variable to pass data into the Thread.
 *  @return void
 */
void Interface_CycleDisplay(void *pData);

/*! @brief ISR for Switch 1.
 *
 */
void __attribute__ ((interrupt)) SW1_ISR(void);
#endif
