/*! @file
 *
 *  @brief Controls the tariffs of the DEM.
 *
 *  Provides tariff information depending on user selection.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef TARIFF_H
#define TARIFF_H

#include "types.h"

typedef enum
{
  TARIFF_MODE_1 = 1,
  TARIFF_MODE_2 = 2,
  TARIFF_MODE_3 = 3
}TTariff;

/*! @brief Initializes the tariff module before first use.
 *
 *  @return BOOL - TRUE if initialization was successful.
 */
BOOL Tariff_Init(void);

/*! @brief Sets the tariff mode.
 *
 *  @param tariffMode - The tariff mode to be set.
 *  @return BOOL - TRUE if write to flash was successful.
 */
BOOL Tariff_SetTariff(TTariff tariffMode);

/*! @brief Gets the current price of electricity (kWh).
 *
 *  @param time - Used when in tariff mode 2, calculating tariff based on time.
 *  @return int32_t - Tariff (cents per kWh).
 */
int32_t Tariff_GetTariffCost(uint32_t time);
#endif
