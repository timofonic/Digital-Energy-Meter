/*! @file
 *
 *  @brief Routines for setting up the general purpose debounce module.
 *
 *  This contains functions for debouncing switches, pushbuttons, touch sensitive interfaces etc.
 *
 *  @author BAllen, APope
 *  @date 2015-09-04
 */
/*!
**  @addtogroup DEBOUNCE_module DEBOUNCE module documentation
**  @{
*/
/* MODULE DEBOUNCE */
#include "Cpu.h"
#include "MK70F12.h"
#include "debounce.h"
#include "FTM.h"

//FTM call-back
static void debounceDelayCompleteCallBack(void * nothing);

//Timer used measure 10ms of debounce
static const TFTMChannel DEBOUNCE_TIMER = {
    .channelNb 			        = 1,
    .delayCount 		        = 244, //244 clock ticks: (10ms/(1/24414)) = 244.14
    .timerFunction 		      = TIMER_FUNCTION_OUTPUT_COMPARE,
    .ioType.outputAction 	  = TIMER_OUTPUT_LOW,
    .ioType.inputDetection 	= TIMER_INPUT_OFF,
    .userFunction 		      = debounceDelayCompleteCallBack,
    .userArguments 		      = 0
};

static void (*ButtonPressedFunction)(void *);
static void *ButtonPressedArguments;

/*! @brief Call-back after switch has reached debounce delay.
 *
 * When a delay of 10ms has been achieved this routine will be called and debouncing for a switch has completed.
 */
void debounceDelayCompleteCallBack(void * nothing)
{
  TButtonState buttonState = (GPIOD_PDIR & 0x00000001);  //PORT D, Pin 0
  PORTD_PCR0 |= PORT_PCR_IRQC(10);                       //Enable interrupts to occur again on this switch

  if (buttonState == LOGIC_0)
  {
    //The button was pressed - invoke the call-back
    ButtonPressedFunction(ButtonPressedArguments);
  }
}

void Debounce_Start(void)
{
  FTM_StartTimer(&DEBOUNCE_TIMER);  //Start the 10ms debounce timer
}

BOOL Debounce_Init(void (*userFunction)(void *), void *userArguments)
{
  ButtonPressedFunction   = userFunction;
  ButtonPressedArguments  = userArguments;

  return (FTM_Set(&DEBOUNCE_TIMER));
}
/* END DEBOUNCE */
/*!
** @}
*/

