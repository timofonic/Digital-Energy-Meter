/*! @file
 *
 *  @brief Prints Meter statistics to the display.
 *
 *  This module contains routines that allows a caller print meter usage stats to the display on the PC interface.
 *
 *  @author APope
 *  @date 2015-09-30
 */
#ifndef DISPLAY_PRINT_H
#define DISPLAY_PRINT_H

#include "types.h"

/*! @brief Prints Time to the PC interface
 *
 *  @param days     - Amount of days that have elapsed (0 - 99)
 *  @param hours    - Amount of hours that have elapsed in that day (0 - 23)
 *  @param minutes  - Amount of minutes that have elapsed in that day (0 - 60)
 *  @param seconds  - Amount of seconds that have elapsed in that day (0 - 60)
 *  @return void
 */
void DisplayPrint_Time(uint8_t days, uint8_t hours, uint8_t minutes, uint8_t seconds);

/*! @brief Prints Power to the PC interface (kW)
 *
 *  @param whole    - whole part PPP
 *  @param fraction - fractional part ppp
 *  @return void
 */
void DisplayPrint_Power(uint16_t whole, uint16_t fraction);

/*! @brief Prints Energy consumption to the PC interface (kWh)
 *
 *  @param whole    - whole part PPP
 *  @param fraction - fractional part ppp
 *  @return void
 */
void DisplayPrint_Energy(uint16_t whole, uint16_t fraction);

/*! @brief Prints running cost to the PC interface ($$$$.cc)
 *
 *  @param dollars - Dollars
 *  @param cents   - Cents
 *  @return void
 */
void DisplayPrint_Cost(uint16_t dollars, uint8_t cents);
#endif
