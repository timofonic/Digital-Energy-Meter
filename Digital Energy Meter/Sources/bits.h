/*! @file
 *
 *  @brief Macros for bit manipulation.
 *
 *  This header file contains some helpful macros for bit manipulation of variables.
 *
 *  @author APope
 *  @date 2015-08-20
 */

#ifndef BITS_H
#define BITS_H

#define SET_BIT(x) 1 << (x)				//Create a variable of bit length (x+1) and set the MSB to a 1, with the rest as zeros
							//Example use, setting bit 7 in temp; temp |= SET_BIT(7)
#define CLEAR_BIT(x) (~(1 << (x)))			//Create a variable of bit length (x+1) and set the MSB to a 0, with the rest as ones
							//Example use, clearing bit 7 in temp; temp &= CLEAR_BIT(7)
#define CHECK_BIT_SET(var, pos) ((var) & (1 <<(pos)))   //Will evaluate to 1 if bit (pos) is set in (var)

#endif
