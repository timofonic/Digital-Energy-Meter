/*! @file
 *
 *  @brief Retrieves samples from the DAC (Digital to Analog Converter).
 *
 *  This module contains the routines that take samples from the current and voltage waveforms.
 *
 *  @author APope
 *  @date 2015-09-30
 */
/*!
**  @addtogroup SAMPLER_module SAMPLER module documentation
**  @{
*/
/* MODULE SAMPLER */
#include "Cpu.h"
#include "DAC.h"
#include "PIT.h"
#include "sampler.h"

int16_t Sampler_VoltageSample;
int16_t Sampler_CurrentSample;

static void (*UserFunction)(void *);
static void *UserArguments;

void samplerCallBack(void * nothing)
{
  //Triggers on the PIT every 800Hz
  static uint16_t voltageSamplePos = 0;  //Will start at sine(0) of the LUT
  static uint16_t currentSamplePos = 0;

  DAC_GetSample(&Sampler_VoltageSample, &Sampler_CurrentSample, voltageSamplePos, currentSamplePos);

  //We traverse the array at multiples of 4 (to get the desired 16 samples within the 64).
  voltageSamplePos = (voltageSamplePos + 4) % 64;
  currentSamplePos = (currentSamplePos + 4) % 64;

  //Invoke the User callback function
  UserFunction(UserArguments);
}

BOOL Sampler_Init(void (*userFunction)(void *), void *userArguments)
{
  //16 samples per period; -> 50Hz waveform -> sampling period of: (800Hz or 1.25mS)
  BOOL initOk = bFALSE;

  if (PIT_Init(CPU_BUS_CLK_HZ, samplerCallBack, 0))
  {
    UserFunction = userFunction;
    UserArguments = userArguments;

    PIT_Set(1250000, bTRUE);
    return (bTRUE);
  }

  return (bFALSE);
}
/* END SAMPLER */
/*!
** @}
*/
