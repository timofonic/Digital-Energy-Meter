# Digital-Energy-Meter
An energy meter is a device that measures the total energy consumption over a certain time interval, on an electricity service. This DEM is a real-time 'multi-threaded' embedded system - designed to operate on the Freescale Tower - also know as the 'TWR-K70F120M-KIT'. This project was developed from specs stipulated by the UTS subject 'Embedded Software'.

Communication with the Freescale tower is accomplished via PC through USB, utilising the tower to PC interface which can be found here <http://services.eng.uts.edu.au/pmcl/embsw/Downloads/TowerPC.exe>.

Please see the pdf document for further explanation of the project requirements. Also note, that this project is best compiled using the 'Kinetis Design Studio'. <http://www.freescale.com/tools/software-and-tools/run-time-software/kinetis-software-and-tools/ides-for-kinetis-mcus/kinetis-design-studio-integrated-development-environment-ide:KDS_IDE>

The tower to PC interface, the 'OS.h' library, and the project specs are under copyright UTS, October 23 2015.
